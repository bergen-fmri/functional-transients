# Windowed Time-Series Analysis of Functional MRI Data

2020-03-03 Alexander R Craven, Department of Biological and Medical Psychology, University of Bergen

This is experimental software; it is presented to elucidate our methodology and with the hope to facilitate replication, but with ABSOLUTELY NO WARRANTY; for details see the [LICENSE](LICENSE) file.

----

## Contents

This repository includes two main functions:

  - [Random Permutation Analysis of 4D Functional Data](#fast-random-permutation-analysis-of-4d-functional-data)
  - [Sampling of functional data across a common temporal window](#sampling-of-functional-data-across-a-common-temporal-window)

----

## Fast Random Permutation Analysis of 4D Functional Data

### Background

While tools such as [FSL PALM](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/PALM) offer impressive functionality and adaptability to different data types, our particular use-case involving hundreds of short 4D datasets places particular demands on system resources, which were better served with an implementation optimized for the specific task.

### Implementation

Our tool is [implemented in C](scripts/r4d/r4d.c) (with innermost loops written in inline assembly). It requires a somewhat recent CPU (see [below](#System-requirements)), and has only been tested on Linux with gcc (version >= 4.6).

Source code may be found under [scripts/r4d](scripts/r4d/).

### System requirements

Your CPU must support AVX2 and FMA3 features; most contemporary mid-range x86-64 processors should manage; Xeon recommended (many cores and a decent cache). The program will try to adapt to your system's physical memory capacity, but more is (almost) always better. The practical minimum is probably 16Gb.

Performance is vastly improved by holding all individual source image files open throughout the process; however, the default configuration of many systems defines a rather conservative limit on the maximum number of open file handles. If possible, adjust this setting upwards (NOFILE or similar, which may be defined in one or more of the system files: /etc/security/limits.conf /etc/systemd/system.conf /etc/systemd/user.conf)

### Compilation

The included [Makefile](scripts/r4d/Makefile) should handle the basic compilation process. Note that this requires [nifticlib](http://niftilib.sourceforge.net/) and [fslio](https://nifti.nimh.nih.gov/pub/dist/src/fsliolib/)

### Usage

```
./r4d performs random permutation analysis between two sets of 4D imaging data.

Usage: ./r4d <mask> <prefix> <list1> <list2>

mask    : Filename for input mask image (eg: mask.nii.gz)
prefix  : Filename prefix for output imagery (eg: r4d_output)
list1   : Text file containing list of source images, one per line,
          for the first group (eg: list1.txt)
list2   : Text file containing list of source images for the second group

Source images must be in NIFTI format (optionally gzipped), having identical dimensions with 32-bit float (4-byte) samples.
```

#### Outputs

Up to twelve output images will be produced (written after processing of each completed volume); these are identified by number and a short-form description, prefixed with the `prefix` parameter described above.

```
 0, 1  Test statistic (group mean) for group 0, group 1
                                 
 2     Distribution mean 
    3  Distribution SD   
                                 
 4, 5  Difference (statistic-dist mean) for group 0, group 1
                                 
 6, 7  P-value from random permutation testing, one-sided
                                 
 8, 9  P-value from gamma approximation of the distribution
                                 
10,11  Q (1-P) value equivalents for 8,9. These two are for convenience of
       visualisation only; due to the nature of the floating-point
       representation, numerical precision may be lower than for the
       corresponding P-value outputs. 
```

### Acknowledgements

The statistical reporting in our implementation draws heavily on the original MATLAB/Octave implementation of FSL PALM, particularly [palm\_moments.m](https://github.com/andersonwinkler/PALM/blob/master/palm_moments.m) and [palm\_gamma.m](https://github.com/andersonwinkler/PALM/blob/master/palm_gamma.m)

- Winkler AM, Ridgway GR, Webster MA, Smith SM, Nichols TE. Permutation inference for the general linear model. NeuroImage, 2014;92:381-397  <https://doi.org/10.1016/j.neuroimage.2014.01.060>
- Winkler AM, Ridgway GR, Douaud G, Nichols TE, Smith SM. Faster permutation inference in brain imaging. Neuroimage. 2016 Jun 7;141:502-516  <https://doi.org/10.1016/j.neuroimage.2016.05.068>

----

## Sampling of functional data across a common temporal window

### Background

Local routines implemented in Python for weighted/interpolated sampling and extraction of subsets of 4D volumetric timeseries data at arbitrary timepoints. This provides two main functions:

- Allows for the extraction of specific periods of interest (for example: individual blocks of activation, transients surrounding the onset and offset of blocks, or transients surrounding discrete events) from a longer series.

- May additionally facilitate joint analysis of functional data from multiple studies, acquired at different TRs. A typical use-case would be in the first stage of a dual-regression ICA (ie, for generation of group spatial maps), for later regression against unfiltered data.

Usually this would be performed late in the processing pipeline, after any motion correction, artifact rejection, and any other temporal filtering. In our study, output from [ICA-AROMA](https://fcp-indi.github.io/docs/user/aroma_ica.html) (also [here](https://doi.org/10.1016/j.neuroimage.2015.02.064)) was taken as input to this function.

### Usage

```
retime.py performs temporal sampling of 4D functional data

Usage:

  ./retime.py [tr_target] [timing <block_timing>] [input1] [input2] ...



    tr_target is sampling interval for output data, in seconds (eg, 1.5).
        Default 1.0; should be specified before timing and source imagery

    block_timing is the name of a CSV or space-separated text file, describing
        blocks or events. Columns are onset, duration, value, one block (or
        event) per line.

    input* are 4D NII or NII.GZ files

For example:

  ./retime.py 1.5 filtered_func_data.nii.gz

  Resample filtered_func_data to 1500ms interval

Another example:

  ./retime.py timing blocks.txt filtered_func_data.nii.gz

  Extract blocks (and transients around onset and offset of blocks) from
  filtered_func_data, on default 1000ms interval, with timing described in
  blocks.txt (FSL 3-column format: onset duration value
```

## Acknowledgements

This software was developed under a project supported by the European Research Council Advanced Grant 693124.
