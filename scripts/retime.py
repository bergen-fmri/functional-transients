#!/usr/bin/python
#
# Temporal resampling and time-window extraction for functional MRI data.
#
# This code provides functions for (weighted/interpolated) sampling and
# extraction of subsets of 4D volumetric timeseries data at arbitrary
# timepoints. 
#
# This may be used to extract specific periods of interest (for example,
# transients surrounding the onset or offset of blocks, or individual events)
# from a longer sequence.

# Additionally, it may be used to facilitate joint analysis of functional data
# from multiple studies, acquired at different TRs. A typical use-case would be
# in the first stage of a dual-regression ICA (ie, generation of group spatial
# maps), for later regression against unfiltered data.
#
# This should be run late in the processing pipeline (after motion correction,
# artifact rejection, and any other temporal filtering).


from __future__ import print_function
import nibabel as nib;
import sys;
import numpy as np;
import re;
import os;
import shutil;
import random;

tr_target=1.0; # default samping interval

debug=0;


def gauss(x,a,x0,sigma): # {{{
  """Gaussian distribution

  Return gaussian distribution scaled by amplitude a, centred around x0, with
  standard deviation sigma, evaluated at points in x (a numpy array)
  """
  return a*np.exp(-((x-x0)**2)/(2*sigma**2));
# }}} gauss


def retime(fn_input,fn_output,tr_target=None,windows=None,remask=True): # {{{
  """Extract arbitrary subsets from 4D timeseries data

  fn_input     : source functional data: in NIFTI format, normally preprocessed

  fn_output    : template for output filename

  tr_target    : Regular sampling interval, for the case where individual
                 samples are unspecified (see windows, next)

  windows      : If specified, a dict-like object where keys are incorporated
                 into the output filename, and values indicate timepoints (in
                 seconds) at which the source data are to be sampled for each
                 item. If the value is empty (None) for any element, the entire
                 input will be sampled at the specified tr.  Similarly if
                 "windows" is not specified, this will be the default
                 operation.

  remask       : Additionally create (and apply to all output data) a new mask
                 according to the condition input>0 across the entire
                 timeseries. This can help to mitigate motion-related edge
                 artifacts.
  """

  print('Load: %s' % (fn_input,));
  im=nib.load(fn_input);
  tr=im.header['pixdim'][4]
  num_temporal_samples_in=im.header['dim'][4];
  total_time=im.header['dim'][4]*tr;

  tfn_output_0m=None;
  tfn_output_gw=None;

  if windows is None:
    windows={};

  if tr_target is not None:
    windows['default']=None;

  dat=None;

  for window_key in windows: # {{{
    skip_resample=False;
    window_times=windows[window_key];

    if window_times is None: # default: use regular tr-timed samples across full length {{{
      tfn_output=fn_output;
      tfn_output_0m=None;
      tfn_output_gw=None;

      if np.abs(tr-tr_target)<0.001: # actually, do nothing.
        print('No meaningful TR adjustment needed; skipping resample stage.');
        shutil.copyfile(fn_input,tfn_output);
        skip_resample=True; 
        # no resampling, but can't just "continue" here; first need to ensure that mask is written, if needed (far below)...

      num_temporal_samples=int(np.round(total_time*1.0/tr_target));
      this_tr=tr_target;
      # FIXME : in future, also take into account slice timing
      dest_sample_times=np.linspace(0,total_time,num_temporal_samples+1)[:-1]
      print('%d samples (%.2fs) @ TR=%.2f => %.2f samples out @ TR=%.2f (check: %d out)' % (im.header['dim'][4],total_time,tr,num_temporal_samples,tr_target,len(dest_sample_times)));
    
    # regular tr-timed samples  }}}

    else: # explicit sample times specified {{{
      tfn_output=re.sub('(.nii(.gz)?)?$','_sections/%s.nii.gz' % (window_key,),op['fn_input'])
      # zero-mean variant
      tfn_output_0m=re.sub('(.nii(.gz)?)?$','_sections/%s_0m.nii.gz' % (window_key,),op['fn_input'])
      # gaussian window variant
      tfn_output_gw=re.sub('(.nii(.gz)?)?$','_sections/%s_gw.nii.gz' % (window_key,),op['fn_input'])

      try:
        os.makedirs(os.path.dirname(tfn_output));
      except OSError:
        pass;

      print(tfn_output);
      num_temporal_samples=len(window_times);
      dest_sample_times=window_times;
      this_tr=(max(dest_sample_times)-min(dest_sample_times))/(num_temporal_samples-1);
      print(dest_sample_times);
      print('%d samples (%.2fs) @ TR=%.2f => %d samples out @ explicit times (calc TR %0.2f):' % (im.header['dim'][4],total_time,tr,len(dest_sample_times),this_tr));
      if debug:
        print(dest_sample_times);

    # explicit sample times }}}

    src_sample_pts=dest_sample_times/tr;

    if window_times is not None:
      if np.any(dest_sample_times<0):
        print('Window starts before scan; skipping.');
        continue;
      elif np.any(dest_sample_times>total_time):
        if debug:
          print(dest_sample_times);
          print(total_time);
        print('Window ends after scan; skipping.');
        continue;

    if debug:
      print(dest_sample_times);
      print(src_sample_pts);

    src_sample_floor=np.floor(src_sample_pts);

    src_sample_next=src_sample_floor+1;

    right_weights=(src_sample_pts-src_sample_floor);
    left_weights=1-right_weights;

    left_ix= [ min(num_temporal_samples_in-1,int(x))   for x in src_sample_floor ];
    right_ix=[ min(num_temporal_samples_in-1,int(x+1)) for x in src_sample_floor ];

    if debug:
      print(left_weights);
      print(right_weights);
      print(left_ix);
      print(right_ix);
      print(len(left_ix));

    if dat is None and (remask or not skip_resample):

      dat=im.get_data();

      if debug:
        print(dat.shape)

      mean=np.mean(dat,axis=3,keepdims=True);

      if debug:
        print(mean.shape)
        print(dat.shape)

      if remask:
        mask=np.min(dat,axis=3)>0;
        mfn_output=re.sub('(.nii(.gz)?)?$','_mask.nii.gz',op['fn_input']);
        new_header=im.header.copy();
        new_header['pixdim'][4]=1;
        new_header['dim'][4]=1;
        out_img=nib.nifti1.Nifti1Image(mask,None,header=new_header);
        print('Write mask to %s' % (mfn_output))
        nib.save(out_img,mfn_output);

      else:
        mask=np.ones(list(dat.shape)[:3]);

    if skip_resample:
      continue;

    out_shape=list(dat.shape)
    out_shape[3]=num_temporal_samples;

    out=np.zeros(out_shape);

    if tfn_output_gw is not None:
      out_gw=np.zeros(out_shape);

    else:
      out_gw=None;

    for t in range(0,num_temporal_samples):
      if debug:
        print('T=%6.2f (ix %3d %4.2f + ix %3d %4.2f)' % (dest_sample_times[t],left_ix[t],left_weights[t],right_ix[t],right_weights[t],))

      # linear interpolation
      out[:,:,:,t]=((dat[:,:,:,left_ix[t]]*left_weights[t])+(dat[:,:,:,right_ix[t]]*right_weights[t]))*mask;

      if out_gw is not None:
        # optionally, sample with a weighted gaussian kernel
        in_times=tr*np.arange(0,num_temporal_samples_in);
        in_weights=gauss(in_times,1.0,dest_sample_times[t],0.4*this_tr);
        # full weighting like this is slow:
        # out_gw[:,:,:,t]=np.average(dat,axis=3,weights=in_weights);
        # so limit to those frames which contribute a non-trivial amount:
        wlim=1e-3;
        inds=np.where(in_weights>wlim)[0];
        if debug>2:
          print("t=%d/%d, dest sample time=%.1f this_tr=%.1f" % (t,num_temporal_samples,dest_sample_times[t],this_tr));
          print(in_times);
          print(in_weights);
        out_gw[:,:,:,t]=np.average(dat[:,:,:,inds],axis=3,weights=in_weights[inds]);

    new_header=im.header.copy();
    new_header['pixdim'][4]=this_tr;
    out_img=nib.nifti1.Nifti1Image(out,None,header=new_header);
    print('Write image to %s' % (tfn_output,))
    nib.save(out_img,tfn_output);

    if tfn_output_0m is not None:
      # calculate zero-mean image
      print('Write zero-mean to %s' % (tfn_output_0m,))
      out_img=nib.nifti1.Nifti1Image(out-np.tile(mean,(1,1,1,out.shape[3])),None,header=new_header);
      nib.save(out_img,tfn_output_0m);

    if tfn_output_gw is not None:
      # calculate zero-mean image
      print('Write zero-mean gaussian-windowed data to %s' % (tfn_output_gw,))
      out_img=nib.nifti1.Nifti1Image(out_gw-np.tile(mean,(1,1,1,out.shape[3])),None,header=new_header);
      nib.save(out_img,tfn_output_gw);

  # }}} for window in windows

# }}} retime


def read_timing(fn_timing): # {{{
  """Read timing information from file fn_timing

  Files should be in CSV format (.csv) or space-delimited text, describing
  blocks with onset, duration and value columns. Files in FSL's 3-column format
  are acceptable.

  2-column data is interpreted as block,duration pairs (state assumed to be 1),
  UNLESS all values are ==1, in which case these are assumed to be impulses
  (discrete events)
  """

  if re.match('.*\.csv$',fn_timing):
    dat=np.loadtxt(fn_timing,delimiter=',',ndmin=2); # explicit ndmin: don't squeeze single-row files!
  else:
    dat=np.loadtxt(fn_timing,delimiter=' ',ndmin=2);

  if dat.shape[1]==2:
    # No third column?
    # If all column 2 values are "1" then treat these as event impulses rather
    # than blocks; duration zero
    col2vals=np.unique(np.abs(dat[:,1]));

    if len(col2vals)==1 and col2vals[0]==1:
      print('Padding with 0 in mid (duration) column');
      ndat=np.zeros((dat.shape[0],dat.shape[1]+1))
      ndat[:,0]=dat[:,0];
      ndat[:,-1]=dat[:,-1];
      print(ndat);

    else:
      # If the column 2 values are more varied, interpret them as durations;
      # then, assume blocks of state 1
      print('Padding with 1 in final (state) column');
      ndat=np.ones((dat.shape[0],dat.shape[1]+1))
      ndat[:,:-1]=dat;

    return ndat;

  else:

    return dat;
# }}} read_timing


def extract_sections(block_timing,window=10,window_pre=None,window_post=None,interval=1.0,prefix=None): # {{{
  """Return a set of vectors, describing timepoints at which to sample the source data based on the specified timing profile.

  block_timing is 2D array such as that returned by read_timing(...)
  window, window_pre, and window_post define the timespan before and after events over which data will be extracted
  interval defines the temporal sampling interval for the data to be extracted

  For each block in block_timing, the returned set will include vectors:
       block_onset-window_pre : interval : block_onset+window_post
  AND block_offset-window_pre : interval : block_offset+window_post (where block_offset=block_onset+block_duration)
  """

  windows={};

  keys_real=[];
  keys_random=[];

  if window_pre is None:
    window_pre=window;

  if window_post is None:
    window_post=window+5; # extend post window by default (HRF lag)

  has_blocks=not all(block_timing[:,1]==0);

  if has_blocks: # standard case: working with blocks {{{
    states=np.unique(block_timing[:,2]);
    num=0;

    if prefix is None:
      prefix='';

    for state in states:
      print('State: %s' % (str(state),))
      for block in [ b for b in block_timing if b[2]==state]:
        num=num+1;
        block_key='%ss%.1fb%03d' % (prefix,state,num);
        if debug:
          print(block);
        onset=block[0];
        offset=block[1]+onset;
        onset_samples= np.linspace( onset-window_pre, onset+window_post,1+((window_post+window_pre)/interval))
        offset_samples=np.linspace(offset-window_pre,offset+window_post,1+((window_post+window_pre)/interval))
        windows[block_key+'_onset']=onset_samples;
        windows[block_key+'_offset']=offset_samples;
        if debug:
          print(onset_samples)
          print(offset_samples)

  # }}}

  else: # not blocks, but individual events {{{
    # This section is highly experimental, and under continued development

    # For event-related design, also generate a similar number of baseline
    # samples, to use as contrast. Attempt to place these as far as possible
    # from other events.

    duration=60.0; # FIXME use real duration
    tt=np.linspace(window_pre+1,duration-window_post-1,1000);
    dist=999*np.ones(tt.shape); # distance (in time) from nearest event
    num=0;
    nrand=len(block_timing); # how many baseline samples to generate?

    # First, generate sample window for all the real events
    for event in block_timing: # {{{
      state=event[2];
      num=num+1;
      block_key='%ssEVb%03d' % (prefix,num);
      et=event[0];

      dist=np.minimum(dist,np.abs(tt-et));

      samples=np.linspace( et-window_pre, et+window_post,1+((window_post+window_pre)/interval))

      if debug:
        print('Samples:');
        print(samples);

      # if any samples fall out of range, this window cannot be extracted and
      # there's no need for a corresponding baseline

      if any(samples<0):
        nrand-=1;
        continue;

      if any(samples>duration):
        nrand-=1;
        continue;

      if state>0:
        windows[block_key+'_onset']=samples;
      else:
        windows[block_key+'_offset']=samples;

    # for event in block_timing }}}

    dist_threshold=7; # TODO needs tuning

    keys_real=windows.keys();

    random.shuffle(keys_real);

    # Next, generate a corresponding number of baseline events, spaced as far
    # as possible from other events
    for rr in range(0,nrand): # {{{
      # randomize a little (in case we have many similarly-distant samples)
      dist=dist+np.random.randn(*dist.shape);

      xx=np.argmax(dist);
      et=tt[xx];

      if debug:
        print('Allocating baseline event at ix=%d, t=%.1f, dist=%.1f ' % (xx,et,dist[xx]));

      if dist[xx]<dist_threshold:
        print('Event spacing too packed; cropping to %d baseline events' % (rr));
        break;

      dist=np.minimum(dist,np.abs(tt-et));

      num=num+1;
      block_key='%ssEVb%03d' % (prefix,num);

      samples=np.linspace( et-window_pre, et+window_post,1+((window_post+window_pre)/interval))
      kk=block_key+'_random';
      keys_random.append(kk);
      windows[kk]=samples;
    # }}}

    # Finally, trim both sets to have equal lengths
    n_each=min(len(keys_real),len(keys_random));
    windows=dict([(k,v) for k,v in windows.items() if k in keys_real[:n_each]+keys_random[:n_each]])
  # }}} not has_blocks

  return windows;

# }}} extract_sections


if __name__ == "__main__":
  context=None;
  timing=None;
  windows=None;
  operations=[];

  acceptable_usage=False;

  for arg in sys.argv[1:]: # process command-line arguments {{{
    if arg=="timing":
      context="timing";
    elif os.path.exists(arg):
      fn_input=arg;
      if context=="timing":
        timing=read_timing(fn_input);
        print(timing);
        bfn=os.path.basename(fn_input);
        prefix=re.sub(r'(\.ev3col?)\.(txt|csv)$','_',bfn)
        prefix=re.sub(r'\.events\.csv$','_raw_events_',prefix)
        windows=extract_sections(timing,prefix=prefix,interval=tr_target);
        if debug:
          print(bfn);
          print(prefix);
          for window in windows:
            print(window);
        context=None;
      elif context is None:
        fn_output=re.sub('(.nii(.gz)?)?$','_tr%.1f.nii.gz' % (tr_target,),fn_input)
        operations.append({'fn_input':fn_input,'fn_output':fn_output,'tr_target':tr_target});
        print('%s => %s @ TR=%.1f' % (fn_input,fn_output,tr_target));
      else:
        print('Unknown context.'); # this should never happen
        acceptable_usage=False;
    else:
      if re.match('^[0-9.]+$',arg): # this is not a filename, but a target TR
        tr_target=float(arg);
        if (len(operations)>0) or windows is not None:
          # tr_target specified too late to be meaningful
          acceptable_usage=False;
      else:
        print('What is %s?' % (arg))
        acceptable_usage=False;

  if len(operations)==0:
    acceptable_usage=False;

  # }}}

  if not acceptable_usage: # Show usage message {{{

    print("%s performs temporal sampling of 4D functional data\n" % (os.path.basename(sys.argv[0]),), file=sys.stderr);
    print("Usage:\n", file=sys.stderr);
    print("  %s [tr_target] [timing <block_timing>] [input1] [input2] ...\n" % (sys.argv[0],), file=sys.stderr);
    print("\n", file=sys.stderr);
    print("    tr_target is sampling interval for output data, in seconds (eg, 1.5).\n        Default 1.0; should be specified before timing and source imagery\n", file=sys.stderr);
    print("    block_timing is the name of a CSV or space-separated text file, describing\n        blocks or events. Columns are onset, duration, value, one block (or\n        event) per line.\n", file=sys.stderr);
    print("    input* are 4D NII or NII.GZ files\n", file=sys.stderr);

    print("For example:\n", file=sys.stderr);

    print("  %s 1.5 filtered_func_data.nii.gz\n" % (sys.argv[0],), file=sys.stderr);
    print("  Resample filtered_func_data to 1500ms interval\n", file=sys.stderr);

    print("Another example:\n", file=sys.stderr);
    print("  %s timing blocks.txt filtered_func_data.nii.gz\n" % (sys.argv[0],), file=sys.stderr);
    print("  Extract blocks (and transients around onset and offset of blocks) from\n  filtered_func_data, on default 1000ms interval, with timing described in\n  blocks.txt (FSL 3-column format: onset duration value\n", file=sys.stderr);
  # }}}

  else: # received meaningful commands... do as requested:
    for op in operations:
      print(op);
      retime(fn_input=op['fn_input'],fn_output=op['fn_output'],tr_target=op['tr_target'],windows=windows);
