// r4d.c
//
// Fast random permutation analysis on 4D imaging data
//
// Alexander R Craven, University of Bergen, 2020/03
//
// Usage: ./r4dd <mask> <prefix> <list1> <list2>
//
//   mask      : Filename for input mask image (eg: mask.nii.gz)
//   prefix    : Filename prefix for output imagery (eg: r4d_output)
//   list1     : Text file containing list of source images, one per line,
//               for the first group (eg: list1.txt)
//   list2     : Text file containing list of source images for the second group
//
// Source images must be in NIFTI format (optionally gzipped), having identical
// dimensions with 32-bit float (4-byte) samples.


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>

#include <pthread.h>

#include <assert.h>
#include <limits.h>        // INT_MAX
#include <math.h>
#include <stdbool.h>
#include <sys/time.h>      // performance profiling
#include <stdalign.h>      // alignas

#include <unistd.h>        // sysconf, _SC_*
#include <sys/resource.h>  // getrlimit (NOFILE)


// NIFTI I/O
// http://niftilib.sourceforge.net/
// https://nifti.nimh.nih.gov/pub/dist/src/fsliolib/
#include <nifti1.h>
#include <fslio.h>

// GNU scientific library, for working with the gamma distribution: https://www.gnu.org/software/gsl/
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf_gamma.h>


// Debug verbosity
int dbg_verbosity=2;

////////////////////////////////////////////////////////////////////////////////
// Performance options:                                                      {{{
//
// PERM_FUNCTION
//
// Nested permutation loops (where all the real work happens) can operate in a
// few different ways, to obtain numerically equivalent outcomes. Pure C
// implementations are included as a practical, readable "reference"
// implementation; optimized inline assembly implementations offer significant
// performance gains.
//
// perm_ix_inner            : Pure C implementation, with voxel index in the innermost loop; intuitive, but generally not optimal.
// perm_sub_inner           : Pure C implementation, with subject in the innermost loop; generally superior performance over perm_ix_inner.
// perm_sub_inner_vfmadd    : Equivalent to perm_sub_inner, but with a mixed C/AVX2 ASM implementation. This gives best performance in most cases, and supports weighted selection (ie, multiplication by factors other than -1 and 1). If in doubt, choose this one.
// perm_sub_inner_vandps    : Experimental version similar to perm_sub_inner, using bitwise vector operations. Binary selection only. Marginal performance gains in some contexts, but generally not worthwhile.
// perm_sub_inner_vblendvps : Experimental version similar to perm_sub_inner, using variable blend operation.  Binary selection only. As with perm_sub_inner_vandps, this might offer marginal gains in some cases, but generally not worthwhile
//

#define PERM_FUNCTION perm_sub_inner_vfmadd

// BACKGROUND_LOAD : an additional thread pre-loads data for the next chunk to
// be analysed.
//
#define BACKGROUND_LOAD


// CACHE_HANDLES : how many file handles (for source imagery) to hold open
// between chunks?  Setting this greater than your maximum number of source
// images avoids much of the overhead of re-opening and seeking within image
// files (which can be substantial, particularly for compressed sources)
//
// Unset to re-open each file for each chunk. This will free up some system
// resources, but may cost a significant amount of time.
//
#define CACHE_HANDLES 2048

// MAX_THREADS : maximum meaningful number of parallel processing threads
// (actual number used will often be slightly lower). The default is the
// system's reported number of cores, according to
// (sysconf(_SC_NPROCESSORS_ONLN))
//
#define MAX_THREADS (sysconf(_SC_NPROCESSORS_ONLN))

// }}}

////////////////////////////////////////////////////////////////////////////////
// Debugging options:                                                        {{{

// FAKE_THREADS : for convenience of debugging, distribute work as if using
//                multiple threads, but don't actually use multiple threads.
//
// #define FAKE_THREADS


// SHOW_DEBUG_INFO : Show extra debug info?
//
// #define SHOW_DEBUG_INFO


// AC_QUICK enables a "quick" testing mode; currently just implies reduced
// number of permutations
//
// #define AC_QUICK


// CHECK_LOAD_CONSISTENCY re-verifies source data in memory against source data
// on disk, to ensure continued consistency (non-trivial in multi-threaded
// context with background loader)
//
// #define CHECK_LOAD_CONSISTENCY


// CHECK_PERM_CONSISTENCY repeats permutation analysis for each section, using
// a "reference" implementation. This is intended for validating optimised
// implementations.
//
// #define CHECK_PERM_CONSISTENCY


// INJECT_VARIATIONS and INJECT_VARIATIONS_BLATANTLY take a copy of data from
// list1, and introduce respectively subtle and not-so-subtle patterns of
// numeric variations to a subset of data, yielding predictable, visible
// results for inspection.
//
// #define INJECT_VARIATIONS
// #define INJECT_VARIATIONS_BLATANTLY

// }}}

////////////////////////////////////////////////////////////////////////////////
// Preamble                                                                  {{{

// CHECK_PERM_CONSISTENCY can only work with a single processing thread
//
#ifdef CHECK_PERM_CONSISTENCY
#define FAKE_THREADS
#endif


// Optimised permutation analysis currently makes use of 256-bit Advanced
// Vector Extensions (AVX2) and FMA3, which require a somewhat recent CPU and a
// somewhat recent compiler (eg: gcc >= 4.6)
//
// To check availability on the local machine (using gcc), try :
// gcc -march=native -dM -E - < /dev/null | grep -ie 'avx\|sse'
//
#ifndef __AVX2__
  #error This program must be compiled with AVX2 support ( something like: -march=native -mfpmath=sse -mavx2 )
#endif


// We use aligned variants of the vector extensions; define alignment grid and
// support functions:

#define ALIGN_AS 64
#define GRANULAR_ROUNDUP(x,y) ((((x)%(y))>0)?((x)+(y)-((x)%(y))):(x))
#define PADDED_SIZE(x)        GRANULAR_ROUNDUP(x,ALIGN_AS)


// How many float values can be processed at a time? 256-bit instructions with
// 32-bit float operands:

#define ATATIME 8


// Refer to the description for PERM_FUNCTION, above
enum perm_modes {
  perm_ix_inner             = 2,
  perm_sub_inner            = 3,
  perm_sub_inner_vfmadd     = 0, /* If in doubt, just use this. */
  perm_sub_inner_vandps     = 7,
  perm_sub_inner_vblendvps  = 4
};


// Error handling macros {{{
// (borrowed from pthread sample code; not consistently used...)
#define handle_error_en(en, msg) \
       do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#define handle_error(msg) \
       do { perror(msg); exit(EXIT_FAILURE); } while (0)
// }}}

// preamble }}}

////////////////////////////////////////////////////////////////////////////////
// Function prototypes and struct declarations                               {{{

void pusage(char *cmd);

double time_diff_secs(struct timeval *tv_in, struct timeval *tv_out);

int n_handles_available();

char ** read_namelist(const char *fname);

// Permutation analysis parameters {{{
struct perm_info {
  int            N_perms;          // number of permutations requested
  int            N_perms_padded;   // number of permutations requested, prefixed with N_conditions real conditions, padded (at end) to ALIGN_AS
  int            N_conditions;     // number of conditions

  int            N_samples;        // eg, number of subjects
  int            N_samples_padded; // number of samples, padded (at end) to ALIGN_AS
  int            subs_inner;       // true if samples in inner loop; false if perms in inner loop; see description for PERM_FUNCTION above

  int           *perms_i;          // permutations, as int32 (1/0)
  float         *perms_f;          // permutations, as float32 (+1/-1 or weighting factor)
  unsigned int  *perms_m32;        // permutations, as mask (0xffffffff or 0x00000000)
};

struct perm_info * perm_info_build(int N_perms, int N_samples, int N_conditions);
static inline void perm_info_set(struct perm_info *pi, int pp, int ps, int pv);
void perm_info_free(struct perm_info * pi);
// }}}

// Distribution and statistics {{{
struct stat_result {
  double         sample;
  int            N;
  double         mean;
  double         sd;
  double         variance;
  double         skewness;
  double         kurtosis;
  double         p;
  double         p_tail;
};

int perm_stats_worker(struct stat_result *stats, float X, long N, float *samples, int buf_ix, int buf_tot, int sample_step, int debug);
int perm_stats(struct stat_result *stats, float X, long N, float *samples);
int perm_stats_cleanup();
// }}}

// NIFTI I/O {{{
struct loader_thread_info {
  pthread_t      thread_id;        // thread ID
  int            num_subjects;     // number of subjects (should be equal to number of entries in nl1+nl2)
  char         **nl1;              // list of source file names (eg, from read_namelist) for the first group
  char         **nl2;              // list of source file names (eg, from read_namelist) for the second group
  int            cur_vol;          // volume from which to read
  int            total_volumes;    // total number of volumes expected
  float         *all_subject_data; // a buffer sufficiently large to hold num_subjects*vol_pts_inmask values
  long           vol_pts_inmask;   // number of non-zero points in the mask (equal to the number of points which will be loaded for each subject)
  float         *mask;             // mask image, of same dimensions as the items in nl1, nl2, with vol_pts_inmask non-zero points
  bool           validate;         // enable "validation" mode for debugging, where existing data in the all_subject_data output buffer is compared with equivalent data on disk.
};

float * read_volume_masked(const char *fname, float *out, float *mask, long output_pts, int vol, int ix);
static void * loader_batch_run(void *arg);
// }}}

// function prototypes }}}

////////////////////////////////////////////////////////////////////////////////
// Miscellaneous helper functions                                            {{{


double time_diff_secs(struct timeval *tv_in, struct timeval *tv_out) { // {{{
  // time_diff_secs : return time difference between two timeval structs
  // (tv_out-tv_in), as a real number of seconds
  return 1e-6*((double)(tv_out->tv_usec-tv_in->tv_usec))+(tv_out->tv_sec-tv_in->tv_sec);
} // }}} time_diff_secs


int n_handles_available() { // {{{
  // n_handles_available, return the reported number of available file handles,
  // at kernel level, according to /proc/sys/fs/file-nr
  //
  // Although file-nr may in fact contain 64-bit unsigned ints, return value
  // will be capped to INT_MAX

  const char * PROC_SYS_FS_FILE_NR="/proc/sys/fs/file-nr";

  FILE *f = fopen(PROC_SYS_FS_FILE_NR, "r");

  if (!f) {
    fprintf(stderr, "Failed to open file: `%s'\n", PROC_SYS_FS_FILE_NR);
    return 1;
  }

  // Values from file-nr may well be 64-bit unsigned ints....
  unsigned long long file_nopen;
  unsigned long long file_nunused;
  unsigned long long file_nmax;

  if (fscanf(f, "%llu%llu%llu", &file_nopen, &file_nunused, &file_nmax) != 3) {
    fprintf(stderr, "Failed to read from file: `%s'\n", PROC_SYS_FS_FILE_NR);
    fclose(f);
    return 1;
  }

  // printf("%llu %llu %llu", file_nopen, file_nunused, file_nmax);

  fclose(f);

  unsigned long long file_avail=file_nmax-file_nopen;

  return (file_avail>INT_MAX)?INT_MAX:(int)file_avail;
} // }}} n_handles_available


char ** read_namelist(const char *fname) { // {{{
  // read_namelist
  // Read a list of filenames from a single source file (fname), plain text,
  // one filename per line.
  //
  // Returns a NULL-terminated list of pointers to names (as nul-terminated
  // strings), the entire thing allocated as a single block.
  //
  // in hindsight, a C++ STL string vector could have made more sense...
  FILE *f = fopen(fname, "rb");
  if (f==NULL) {
    printf("couldn't open file %s\n", fname);
    return NULL;
  }
  printf("reading %s (%p)\n",fname,(void *)f);

  // get size, then rewind
  fseek(f, 0, SEEK_END);
  long fsize = ftell(f);
  /* rewind */
  rewind(f);
  fseek(f, 0, SEEK_SET);

  char          *buf=malloc(fsize+1);

  fread(buf, 1, fsize, f);
  fclose(f);
  f=NULL;

  buf[fsize] = 0;

  char         **buf2=NULL;
  int            num_lines=0;
  int            line_num=0;
  char          *po=NULL;

  // pass 0 : count lines to determine required buffer size
  // pass 1 : create indexed buffer and populate with lines

  for (int pass=0;pass<2;pass++) {
    if (pass==1) {
      buf2=malloc(fsize+1+(num_lines+1)*sizeof(char *));
      po=(char *)((char **)(buf2+(num_lines+1)));
    }
    line_num=0;
    int empty=true;
    for (char *p=buf;*p!='\0';p++) {
      if ((*p=='\r')||(*p=='\n')) {
        if (!empty) {
          line_num++;
          if (pass>0) {
            *(po++)='\0'; /* nul-terminate output copy */
          }
          empty=true;
        }
      }
      else {
        if (empty) {
          if (pass==1) {
            buf2[line_num]=po;
          }
          empty=false;
        }
        if (pass==1) {
          *(po++)=*p;
        }
      }
    }
    if (pass==0) {
      num_lines=line_num;
    }
    else {
      printf("line num %d num lines %d\n", line_num, num_lines);
      assert(line_num=num_lines);
    }
  }

  assert(buf2!=NULL);
  buf2[line_num]=NULL; // terminate the list

  free(buf);
  return buf2;

} // }}} read_namelist


// }}} various helper functions

////////////////////////////////////////////////////////////////////////////////
// NIFTI IO                                                                  {{{

float * read_volume_masked(const char *fname, float *out, float *mask, long output_pts, int vol, int ix) { // {{{
  // Read a single volume from fname (a nifti file).
  //
  // If "out" vector is specified, data will be written there. Only meaningful
  // if mask and output_pts are also specified. Returned pointer will be equal
  // to "out".
  //
  // If "out" vector is NULL, a new aligned buffer will be allocated -- either
  // of size output_pts if mask and output_pts are specified, or the full size
  // of the input volume otherwise. This will be the returned pointer; it ought
  // to be free'd when no longer needed.
  //
  // If no mask is specified, output data will be populated with one volume, specified by the vol parameter.
  //
  // If a mask is specified, returned data will be a flat vector of all items
  // from the specified volume, for which the corresponding element of mask is
  // non-zero.
  //
  // If CACHE_HANDLES is specified, ix is an application-specific identifier
  // (>0 and <CACHE_HANDLES) used to uniquely identify this image file; file
  // handle and pointer will be held for re-use in subsequent calls. This can
  // significantly reduce overheads, especially when working with compressed
  // data.
  FSLIO         *fslio=NULL;
  FSLIO         *fslioh=NULL;
  static void   *vbuf=NULL;
  static long    vbuf_bytes=-1;
  static pthread_mutex_t mutex;
  long           input_pts=0;
  int            cur_vol=0;

#ifdef CACHE_HANDLES
  static int     cache_handles=CACHE_HANDLES;
  static FSLIO  *cached_fslio[CACHE_HANDLES];
  static int     cached_ix[CACHE_HANDLES];
  static long    cached_pts[CACHE_HANDLES];
  static bool    cache_initialized=false;

  if ((ix>=0) && (ix<cache_handles)) {
    // first, ensure that the cache has been properly initialized
    if (!cache_initialized) {
      pthread_mutex_lock(&mutex);
      if (!cache_initialized) {
        for (int x=0;x<CACHE_HANDLES;x++) {
          cached_fslio[x]=NULL;
          cached_pts[x]=0;
          cached_ix[x]=0;
        }
        // lower cache_hanldes if rlimit stops us...
        struct rlimit flim;
        getrlimit(RLIMIT_NOFILE, &flim);
        if (flim.rlim_cur<(11*(unsigned long)cache_handles)/10) { // calculate with a 10%-ish margin
          printf("system rlimit on open files prevents full-sized cache!\n");
          cache_handles=(10*flim.rlim_cur)/11;
        }

        cache_initialized=true;
      }
      pthread_mutex_unlock(&mutex);
    }
    pthread_mutex_lock(&mutex);
    assert(cache_initialized);
    fslio=cached_fslio[ix];
    if (fslio!=NULL) printf("*");
    input_pts=cached_pts[ix];
    cur_vol=cached_ix[ix];
    pthread_mutex_unlock(&mutex);
  }
#endif

  if (vol<0) vol=0;

  if (fslio==NULL) {

    fslioh = FslReadHeader(fname); // might raise -Wdiscarded-qualifiers: for some reason, FslReadHeader does not declare fname as const

    if (fslioh == NULL) {
      fprintf(stderr, "\nError, could not read header info for %s.\n",fname);
      exit(1);
    }

    input_pts=FslGetVolSize(fslioh);

    assert(fslioh->niftiptr->nbyper==4); // can only deal with float32 values for now...

    if ((mask==NULL)||(output_pts==0)) {
      assert(out==NULL); // pre-allocated output buffer only meaningful with a mask and known size
      output_pts=input_pts;
      mask=NULL;
    }
  }

  if (out==NULL) {
    out=aligned_alloc(ALIGN_AS, output_pts*sizeof(float));
  }

  if (out == NULL) {
    fprintf(stderr, "\nError, no output buffer for reading %s.\n",fname);
    exit(1);
  }

  if (fslio==NULL) {
    fslio=FslOpen(fname,"r");
  }

  if (vol!=cur_vol) {
    FslSeekVolume(fslio,vol);
    cur_vol=vol;
  }

  if (mask==NULL) {
    // no mask, just read straight into the output buffer
    FslReadVolumes(fslio,out,1);
    cur_vol++;
  }
  else {
    // masking. first load into a temporary buffer
    long input_bytes=input_pts*fslio->niftiptr->nbyper;

    pthread_mutex_lock(&mutex);

    if (input_bytes>vbuf_bytes) {
      printf("Need %ld buffer (had %ld).\n", input_bytes, vbuf_bytes);
      if (vbuf!=NULL) {
        free(vbuf);
      }
      vbuf=aligned_alloc(ALIGN_AS, input_bytes);
      vbuf_bytes=input_bytes;
    }

    if (vbuf == NULL) {
      fprintf(stderr, "\nError, could not allocate temp memory for reading %s.\n",fname);
      exit(1);
    }

    FslReadVolumes(fslio,vbuf,1);
    cur_vol++;

    float * fp_in=(float *)vbuf;
    float * fp_out=(float *)out;
    float * fp_mask=(float *)mask;
    long  out_remaining=output_pts;
    for (long i=0; i<input_pts; i++, fp_mask++, fp_in++) { /* apply the mask */
      if (*fp_mask) {
        *(fp_out++)=*fp_in;
        out_remaining--;
      }
    }

    pthread_mutex_unlock(&mutex);

    assert(out_remaining==0);

    bool found_a_number=false;
    for (int ix=0;ix<output_pts;ix++) {
      if ((out[ix]!=NAN)&&(fabs(out[ix])>1e-9)) {
        found_a_number=true;
        break;
      }
    }
    if (!found_a_number) {
      printf("No meaningful data found in %s\n", fname);
      assert(found_a_number);
    }
  }

#ifdef CACHE_HANDLES
  // If there's room in our cache struct, and the system has abundant file
  // handles to spare, hold this one open for later.
  if ((ix>=0)&&(ix<CACHE_HANDLES)&&(n_handles_available()>3000)) {
    cached_fslio[ix]=fslio;
    cached_pts[ix]=input_pts;
    cached_ix[ix]=cur_vol;
    fslio=NULL;
  }
#endif

  if (fslio!=NULL) {
    FslClose(fslio);
  }

  return out;

} // read_volume_masked }}}

static void * loader_batch_run(void *arg) { // {{{
  // Read a batch of data (a single volume, from every source image) as
  // specified by the supplied loader_thread_info struct
  struct loader_thread_info * inf=arg;
  int sub=0;

  char **lists[2];
  lists[0]=inf->nl1;
  lists[1]=inf->nl2;

#ifdef INJECT_VARIATIONS
  // In this debugging mode, use same source data for each group (this data will be perturbed systematically later)
  lists[1]=inf->nl1;
#endif

  for (int ln=0;ln<2;ln++) {
    for (char **np=lists[ln];*np!=NULL;np++) {
      assert(sub<inf->num_subjects);
      if (inf->cur_vol<0) {
        FSLIO * tmp=FslReadHeader(*np);
        assert(inf->total_volumes==tmp->niftiptr->nt);
      }
      else {
        if (inf->validate) {
          float * buf=read_volume_masked(*np, NULL, inf->mask, inf->vol_pts_inmask,inf->cur_vol,sub); // read into a new buffer
          printf("checking sub %d (%s)\n",sub,*np);
          long mismatch=0;
          float * op=inf->all_subject_data+(sub*inf->vol_pts_inmask)+((inf->cur_vol%2)*inf->num_subjects*inf->vol_pts_inmask);
          for (long n=0;n<inf->vol_pts_inmask;n++) {
            if (buf[n]!=op[n]) {
              mismatch++;
            }
          }
          printf("%.05f%% mismatch\n", (100.0*mismatch)/inf->vol_pts_inmask);
          assert(0==memcmp(buf, inf->all_subject_data+(sub*inf->vol_pts_inmask)+((inf->cur_vol%2)*inf->num_subjects*inf->vol_pts_inmask), inf->vol_pts_inmask));
          free(buf);
        }
        else {
          read_volume_masked(*np, inf->all_subject_data+(sub*inf->vol_pts_inmask)+((inf->cur_vol%2)*inf->num_subjects*inf->vol_pts_inmask), inf->mask, inf->vol_pts_inmask,inf->cur_vol,sub);

#ifdef INJECT_VARIATIONS
          // for debugging purposes only: inject variations into the source data {{{
          long n_seen=0;
          long n_mod=0;
          float * mp=inf->all_subject_data+(sub*inf->vol_pts_inmask)+((inf->cur_vol%2)*inf->num_subjects*inf->vol_pts_inmask);
          for (int x=0;x<inf->vol_pts_inmask;x++) {
            if ((x<inf->vol_pts_inmask/3) && (ln==0)) { // first 1/3rd for group 0: increase
#ifdef INJECT_VARIATIONS_BLATANTLY
              mp[x]=mp[x]+100; //*1.8;
#else
              mp[x]=mp[x]+10; //*1.04;
#endif
              n_mod++;
            }
            else if ((x>inf->vol_pts_inmask/4)&&(x<inf->vol_pts_inmask/2)&&(ln==1)) { // second 1/4 for group 1: decrease
#ifdef INJECT_VARIATIONS_BLATANTLY
              mp[x]=mp[x]-50; //*0.48;
#else
              mp[x]=mp[x]-5; //*0.98;
#endif
              n_mod++;
            }
            if ((x>(inf->vol_pts_inmask*3)/4)&&(sub%2)) { // last quarter: perturb alternating subjects
#ifdef INJECT_VARIATIONS_BLATANTLY
              mp[x]=mp[x]*1.36;
#else
              mp[x]=mp[x]*1.06;
#endif
              n_mod++;
            }
            n_seen++;
#ifdef INJECT_VARIATIONS_BLATANTLY
            x++; // Stripey output; even more obvious.
#endif
          }
          printf("Injecting variations to %ld/%ld voxels\n", n_mod, n_seen);
          // }}}
#endif
        }
      }
      sub++;
      printf((sub%100==0)?"|":"."); fflush(stdout);
    }
  }
  return NULL;
} // loader_batch_run }}}

// }}}

////////////////////////////////////////////////////////////////////////////////
// Statistic calculation and storage                                         {{{

int perm_stats_worker(struct stat_result *stats, float X, long N, float *samples, int buf_ix, int buf_tot, int sample_step, int debug) { // {{{
  // Calculate permutation t-statistics (for statistic X against N samples)
  //
  // In addition, calculate p-values from permutation distribution,
  // approximated as a gamma distribution, to yield more accurate estimates for
  // lower N
  //
  //       stats : structure for output info
  //           X : test statistic
  //           N : number of samples
  //
  //     samples : buffer of at least N*sample_step samples
  //      buf_ix : thread number, in multi-thread mode (0 otherwise)
  //     buf_tot : number of threads (0 or 1 for single-thread mode)
  // sample_step : step size for indexing samples buffer (eg, Nsamples if
  //               ordering has samples in inner loop)
  //
  // To clean up internal buffers after use, call with use stats=NULL, N=0
  // (other parameters ignored); this is not strictly necessary, but nice to do
  // for completeness. perm_stats_cleanup invokes this mode.
  //
  // This function draws heavily on the Matlab implementation by Winkler et al
  // in FSL PALM, in particular:
  //
  // https://github.com/andersonwinkler/PALM/blob/master/palm_moments.m
  // https://github.com/andersonwinkler/PALM/blob/master/palm_gamma.m
  //
  // Winkler AM, Ridgway GR, Webster MA, Smith SM, Nichols TE. Permutation
  // inference for the general linear model. NeuroImage, 2014;92:381-397
  // https://doi.org/10.1016/j.neuroimage.2014.01.060
  //
  // Winkler AM, Ridgway GR, Douaud G, Nichols TE, Smith SM. Faster permutation
  // inference in brain imaging. Neuroimage. 2016 Jun 7;141:502-516
  // https://doi.org/10.1016/j.neuroimage.2016.05.068

  double         moments_n;
  double         moments_mu;
  double         moments_s2;
  double         moments_m3;
  double         moments_ssq;
  double         moments_sigsq;
  double         moments_gamm1;

  static long    failed_P=0;
  static long    failed_Q=0;
  static long    failed_consistency=0;
  static long    failed_range=0;

  static         pthread_mutex_t mutex;
  static float * buf=NULL;
  static long    last_bs=-1;
  float *        pts_demean;
  int            psf_status=GSL_SUCCESS;

#ifndef SHOW_DEBUG_INFO
  debug=0;
#endif

  int            debug_this=debug;

  if (stats==NULL && N==0) {
    // cleanup requested
    pthread_mutex_lock(&mutex);
    if (buf!=NULL) {
      free(buf);
      last_bs=-1;
      buf=NULL;
    }
    printf("perm_stats_worker, failed P: %ld, failed Q: %ld, detected mismatch: %ld, detected incongruous: %ld\n", failed_P, failed_Q, failed_consistency, failed_range);
    pthread_mutex_unlock(&mutex);
    return -1;
  }

  // sanitize input a bit...

  if (buf_tot<=1) {
    buf_ix=0;
    buf_tot=1;
  }

  assert(buf_ix<buf_tot);
  assert(sample_step>0);


  // Create a buffer for demeaned values; re-use between successive calls if
  // possible, to minimise overhead from (re)allocations
  //
  // buf_ix/buf_tot for multithread usage: ix is thread num, tot is num of threads

  long           bs=N*buf_tot;

  if (last_bs<bs) { // pre-existing buffer too small?
    pthread_mutex_lock(&mutex);
    if (last_bs<bs) { // re-test within mutex lock'd section, to avoid possible race condition
      printf("Allocate buf %ld for %ld*%d stats\n", bs, N, buf_tot);
      if (buf!=NULL)
        free(buf);
      buf=aligned_alloc(ALIGN_AS, bs*sizeof(float));
      last_bs=bs;
    }
    pthread_mutex_unlock(&mutex);
  }

  pts_demean=buf+(N*buf_ix);

  moments_n=(float)N;

  moments_mu=moments_ssq=moments_m3=0;

  // calculate mean
  for (int i=0;i<N;i++) {
    moments_mu+=samples[i];
  }

  moments_mu/=moments_n;

  // rev: true if lower values of the statistic are evidence in favour of the
  // alternative (statistic is sub-mean)
  bool           rev=(X<moments_mu);

  float          p_raw=0;

  for (int i=0;i<N;i++) {
    if (!(X<samples[i*sample_step])!=!rev) {
      p_raw+=1;
    }
  }

  p_raw=p_raw/(float)N;

  stats->sample=X;
  stats->N=N;
  stats->mean=moments_mu;
  stats->p=p_raw;

  // Parametrize the distribution... {{{

  for (int i=0;i<N;i++) {
    pts_demean[i]=samples[i*sample_step]-moments_mu;
    moments_ssq+=pow(pts_demean[i],2);
    moments_m3+=pow(pts_demean[i],3);
  }

  // variance
  moments_sigsq=moments_ssq/(moments_n-1.);

  // skewness...
  moments_s2=moments_ssq/moments_n; // biased variance
  moments_m3/=moments_n;
  moments_gamm1=(moments_m3/pow(moments_s2,1.5))*sqrt((moments_n-1)/moments_n)*(moments_n/(moments_n-2)); // unbiased skewness

  stats->variance=moments_sigsq;
  stats->sd=pow(stats->variance,0.5);
  stats->skewness=moments_gamm1;

  // }}}

  // Derive parameters of gamma distribution (Minas & Montana, 2014) {{{

  double         kpar=4/pow(moments_gamm1,2);
  double         tpar=moments_gamm1/2.0;
  double         cpar=-2.0/moments_gamm1;

  double         gamma_X=(X-moments_mu)/(pow(moments_sigsq,.5));

  int            tail; // 0: lower, 1: upper

  if (rev) {
    if (moments_gamm1>0)
      tail=0;
    else
      tail=1;
  }
  else {
    if (moments_gamm1>0)
      tail=1;
    else
      tail=0;
  }


  double         a=(gamma_X-cpar)/tpar;
  double         Q=0.0;
  double         Qalt=0.0;
  double         kth=1e6;

  // }}}

  gsl_sf_result  Qr;
  int            ret=GSL_SUCCESS;

  // Refer: https://www.gnu.org/software/gsl/doc/html/specfunc.html#incomplete-gamma-functions
  //        https://se.mathworks.com/help/matlab/ref/gammainc.html

  if (tail==1) { // 0: lower (P; matlab default); 1: upper (Q, equivalent to 1-P)
    ret=gsl_sf_gamma_inc_Q_e(kpar,(gamma_X-cpar)/tpar, &Qr);
    if (ret!=GSL_SUCCESS) {
      // For some reason, gsl_sf_gamma_inc_Q_e is a little fragile. gsl_sf_gamma_inc_P_e is generally better-behaved:
      failed_Q++;
      ret=gsl_sf_gamma_inc_P_e(kpar,(gamma_X-cpar)/tpar, &Qr);
      Qr.val=1.0-Qr.val;
    }
  }
  else {
    ret=gsl_sf_gamma_inc_P_e(kpar,(gamma_X-cpar)/tpar, &Qr);
    if (ret!=GSL_SUCCESS) {
      // This doesn't happen.
      failed_P++;
      ret=gsl_sf_gamma_inc_Q_e(kpar,(gamma_X-cpar)/tpar, &Qr);
      Qr.val=1.0-Qr.val;
    }
  }
  Q=Qr.val;

  //if (fabs(Qalt-Q)>1e-3) {
  //  debug_this=true;
  //}

  if (ret==GSL_SUCCESS) {
    if (fabs(p_raw-Q)>0.3) debug_this=true;
  }
  else {
    // return status will reflect the first error encountered.
    if (psf_status==GSL_SUCCESS) {
      psf_status=ret;
    }
    // Handle undefined result...
    debug_this=true;
    if (rev) {
      if (moments_gamm1>0)
        Q=1/(double)N;
      else
        Q=1;
    }
    else {
      if (moments_gamm1>0)
        Q=1;
      else
        Q=1/(double)N;
    }
  }

  if (fabs(p_raw-Q)>0.3) {
    failed_consistency++;
  }

  if (Q<0) {
    failed_range++;
    Q=0;
    debug_this=true;
  }
  else if (Q>1.0) {
    failed_range++;
    Q=1;
    debug_this=true;
  }

  if (debug_this) {

    char        *dir=(tail?"upper":"lower");

    printf("perm buf %2d/%2d (N=%5ld) : [%7.1f] ", buf_ix, buf_tot, N, X);

    for (int dx=0;dx<N&&dx<10;dx++) {
      printf("%7.1f ", samples[dx]);
    }

    printf("|mean %8.2f std %8.2f | X %8.2f (%s) | sk %8.5f k %9.3e c %8.2f t %8.2f a %4.1f", moments_mu,pow(moments_sigsq,0.5),X,dir,moments_gamm1,kpar,cpar,tpar,a);
    printf("|p %6.4f Q %6.4f ", p_raw, Q);

    if (kpar>kth)
      printf(" K! ");

    if (a<=0.0)
      printf(" a! ");

    if (fabs(p_raw-Q)>0.3)
      printf(" Q! ");
    else
      printf("    ");

    if (ret!=GSL_SUCCESS)
      printf(" GSL! %6.4f %.3e alt %6.4f",Qr.val, Qr.err, Qalt);

    printf("\n");
  }

  stats->p_tail=(float)Q;

  return psf_status;

} // }}} perm_stats_worker


int perm_stats(struct stat_result *stats, float X, long N, float *samples) { // {{{
  // Simplified invokation of perm_stats_worker for basic single-threaded
  // operation. See perm_stats_worker comment for description of parameters.
  return perm_stats_worker(stats,X,N,samples,0,1,1,1);
} // }}} perm_stats


int perm_stats_cleanup() { // {{{
  // Release perm_stats_worker's internal buffers
  // (invokes perm_stats_worker in cleanup mode, with null parameters)
  return perm_stats_worker(NULL,0,0,NULL,0,0,0,0);
} // }}} perm_stats_cleanup


// }}}

////////////////////////////////////////////////////////////////////////////////
// permutation info structure declaration and manipulation                   {{{

static inline long perm_info_ix(struct perm_info *pi, int pp, int ps, int extended) { // {{{
  // return index into perm_info perms_* vectors, for permutation number pp and sample ps
  //
  // The first N_conditions cases for pp represent original groupings
  //
  // if "extended" is true, allow indexing into the "padding" sections, rather
  // than real values. This is generally only meaningful during initialisation
  // of the structures (filling with empty data)
  if (extended) {
    assert(pp<pi->N_perms_padded);
    assert(ps<pi->N_samples_padded);
  }
  else {
    assert(pp<pi->N_conditions+pi->N_perms);
    assert(ps<pi->N_samples);
  }

  if (pi->subs_inner) {
    // inner indexing by subject, outer by perm (for use with the perm_sub_inner* PERM_FUNCTION modes)
    return ((long)pp*(long)(pi->N_samples_padded))+(long)ps;
  }
  else {
    // inner indexing by permutation, outer by subject (not currently used)
    return ((long)ps*(long)(pi->N_perms_padded))+(long)pp;
  }
} // }}} perm_info_ix

static inline void perm_info_set_(struct perm_info *pi, int pp, int ps, register int pv, int extended) { // {{{
  // Write equivalent values for pv to each of the perms_* vectors
  register long pix=perm_info_ix(pi,pp,ps,extended);
  pi->perms_i[pix]=pv;
  pi->perms_f[pix]=(float)pv;
  pi->perms_m32[pix]=pv?0xffffffff:0x00000000;
} // }}} perm_info_set_

static inline void perm_info_set(struct perm_info *pi, int pp, int ps, int pv) { // {{{
  // trivial wrapper for perm_info_set_ in standard mode ("standard" means no
  // writing to the padded regions)
  perm_info_set_(pi,pp,ps,pv,false);
} // }}} perm_info_set

struct perm_info * perm_info_build(int N_perms, int N_samples, int N_conditions) { // {{{
  // allocate and populate a perm_info structure with associated vectors
  // describing (randomised) grouping for each sample, for each permutation
  //
  // N_perms     : requested number of permutations
  // N_samples   : number of samples
  // N_conditions: number of conditions
  //
  // Allocated vectors within the returned perm_info structure will include
  // N_conditions slots per sample, indexed as perms 0-(N_conditions-1), for
  // storing the original grouping. These must be filled subsequently,
  // preferably using perm_info_set

  long N_samples_padded=PADDED_SIZE(N_samples);
  long N_perms_padded  =PADDED_SIZE(N_perms+N_conditions);
  long perm_info_struct_size_padded=PADDED_SIZE(sizeof(struct perm_info));

  // use one alloc for the struct and all the permutation vectors
  long total_size=perm_info_struct_size_padded+N_samples_padded*N_perms_padded*(sizeof(int)+sizeof(float)+sizeof(unsigned int));
  struct perm_info * pi=aligned_alloc(ALIGN_AS, total_size);

  assert(pi!=NULL);

  memset(pi,0,total_size);

  if (dbg_verbosity>1) {
    printf("perm_info_build: allocating %ld bytes, for %d perms * %d samples (%d conditions) padded to %ld * %ld\n", total_size, N_perms, N_samples, N_conditions, N_perms_padded, N_samples_padded);
  }

  pi->N_perms          =N_perms;
  pi->N_perms_padded   =N_perms_padded;

  pi->N_conditions     =N_conditions;

  pi->N_samples        =N_samples;
  pi->N_samples_padded =N_samples_padded;

  pi->perms_i          =(void *)(((char *)pi)+perm_info_struct_size_padded);
  pi->perms_f          =(float *)((int *)(pi->perms_i+N_samples_padded*N_perms_padded));
  pi->perms_m32        =(unsigned int *)((float *)(pi->perms_f+N_samples_padded*N_perms_padded));

  pi->subs_inner       =1; // 1: inner index by subject; outer by permutation; 0: vice versa (currently unused)

  // build permutation table; note that this is currently only meaningful for N_conditions==2 {{{

  assert(N_conditions==2);

  int pv;

  for (int ps=0;ps<pi->N_samples_padded;ps++) {
    for (int pp=0;pp<pi->N_perms_padded;pp++) {
      if (ps>=pi->N_samples) { // initialize padded sections with empty values: {{{
        pv=0;
        perm_info_set_(pi,pp,ps,pv,true);
      }
      else if (pp>=(pi->N_perms+pi->N_conditions)) {
        pv=0;
        perm_info_set_(pi,pp,ps,pv,true);
      }
      // }}}
      else { // the usual case of regular (non-padded) indices:
        pv=rand()%N_conditions;
        perm_info_set(pi,pp,ps,pv);
      }
    }
  }

  // }}}

  if (dbg_verbosity>1) {
    printf("perm_info_build: completed successfully.\n");
  }

  return pi;

} /* }}} perm_info_build  */

void perm_info_free(struct perm_info * pi) { // {{{
  // Release the perm_info structure
  // nothing fancy; internal pointers are all within the same alloc'd block
  free((void *)pi);
} // }}} perm_info_free

// permutation info structure }}}

////////////////////////////////////////////////////////////////////////////////
// permutation processing, threaded                                          {{{

struct perm_thread_info {           // Details passed to individual processing threads
   pthread_t     thread_id;         // thread ID, as returned by pthread_create()
   int           thread_num;        // thread number
   int           thread_count;      // total number of threads

   float *       thread_buf_in;
   float *       thread_buf_out;
   unsigned long thread_buf_bytes;  // number of bytes allocated at thread_buf_out

   struct        perm_info * perm;
   enum          perm_modes mode;   // See definition of perm_modes, earlier in this file

   struct        stat_result * sr0; // stat results...
   struct        stat_result * sr1;

   // The following options allowed for multiple permutations (on the same
   // sample data) to be distributed across processing threads. One might
   // expect that this should allow more efficient use of data caches nearer
   // the CPU, and hence improved performance. One could be terribly
   // disappointed...
   int           thread_perm0;      // starting permutation number (normally 0, except when parallelising by perm)
   int           thread_permstep;   // step perm number (number of parallel threads when parallelising by perm)
   int           thread_permstop;   // stop perm number (if taking blocks of perms)
   long          thread_slice_size;

};


static void * perm_processing_thread(void *arg) { // {{{
  // run a batch of permutations for a slice of data, as described by the
  // supplied perm_thread_info struct
  //
  // Statistical outcomes for initial conditions will be written to sr0 and sr1
  // members of the structure, if assigned.
  struct timeval tv_a;
  struct timeval tv_b;
  struct timeval tv_c;
  struct perm_thread_info *tinfo = arg;

  char buf[128];

  gettimeofday(&tv_a,NULL);

  if ((tinfo->thread_permstop<=0)||(tinfo->thread_permstop>tinfo->perm->N_perms_padded))
    tinfo->thread_permstop=tinfo->perm->N_perms_padded;

  register int   ss=tinfo->thread_slice_size;

  register float *p_out=tinfo->thread_buf_out;
  register float *p_in=tinfo->thread_buf_in;

  float         *p_perm_f=NULL;
  unsigned int  *p_perm_m32=NULL;

  alignas(ALIGN_AS) float sum_[ATATIME];
  void          *sum_v;
  int            ixi;
  void          *p_perm_;
  void          *p_in_;

  switch(tinfo->mode) {

    case perm_ix_inner: // simple; slice index in inner loop {{{
      for (register long ix=0;ix<ss;ix++,p_out++) {
        (*p_out)=0;
      }
      for (int perm=tinfo->thread_perm0;perm<tinfo->thread_permstop;perm+=tinfo->thread_permstep) {
        int *p_perm=tinfo->perm->perms_i+perm*tinfo->perm->N_samples_padded;
        for (int sub=0;sub<tinfo->perm->N_samples_padded;sub++) {
          register int mul=p_perm[sub];
          p_in=tinfo->thread_buf_in;
          p_out=tinfo->thread_buf_out;
          for (register long ix=0;ix<ss;ix++,p_in++,p_out++) {
            (*p_out)+=mul*(*p_in);
          }
        }
      }
      break; // }}}

    case perm_sub_inner: // voxel in outer loop; subject inner-most (no need to read back from p_out) {{{
      for (register long ix=0;ix<ss;ix++) {
        register float sum=0;
        p_in=tinfo->thread_buf_in+ix*tinfo->perm->N_samples_padded;
        for (int perm=tinfo->thread_perm0;perm<tinfo->thread_permstop;perm+=tinfo->thread_permstep) {
          sum=0;
          int *p_perm=tinfo->perm->perms_i+perm*tinfo->perm->N_samples_padded;
#ifdef SHOW_DEBUG_INFO
          if (ix==0 && perm<2) { // debug info?
            printf("%03d: ",perm);
            for (int sub=0;sub<tinfo->perm->N_samples_padded;sub++) {
              printf(p_perm[sub]?"X":"-");
            }
            printf("\n");
          }
#endif
          for (int sub=0;sub<tinfo->perm->N_samples_padded;sub++,p_in++) {
            register int mul=p_perm[sub];
            sum+=mul*(*p_in);
          }
          p_in-=tinfo->perm->N_samples_padded; // rewind
          (*(p_out++))=sum;
        }
        if (tinfo->thread_buf_bytes<(p_out-tinfo->thread_buf_out)*sizeof(float)) {
          printf("thread %02d ix %8ld perm %6d p_out orig: %p p_out_cur: %p sz: %ld diff: %ld     (%d:%d:%d * %d)\n",tinfo->thread_num,ix,-1,(void *)(tinfo->thread_buf_out),(void *)p_out,tinfo->thread_buf_bytes,(p_out-tinfo->thread_buf_out)*sizeof(float),tinfo->thread_perm0,tinfo->thread_permstep,tinfo->thread_permstop,ss);
        }
      }
      break; // }}}

    case perm_sub_inner_vfmadd: // equivalent operation to perm_sub_inner with vector-optimized inner loop based on VFMADD instruction {{{
      for (register long ix=0;ix<ss;ix++) {
        sum_v=sum_;
        ixi=tinfo->perm->N_samples_padded;
        p_in=tinfo->thread_buf_in+ix*tinfo->perm->N_samples_padded;

        ixi=ixi-(ixi%ATATIME); // FIXME: currently MUST be a multiple of ATATIME
        assert(ixi%ATATIME==0);
        ixi/=ATATIME;
        assert(ixi>0);

        for (int perm=tinfo->thread_perm0;perm<tinfo->thread_permstop;perm+=tinfo->thread_permstep) { // per permutation {{{
          p_perm_f=tinfo->perm->perms_f+perm*tinfo->perm->N_samples_padded;

          p_perm_=(void *)p_perm_f;
          p_in_=(void *)p_in;

          ixi=tinfo->perm->N_samples_padded/ATATIME;

          // per-subject {{{
          // The following __asm__ block implements a vectorized form of this:
          //
          // for (int sub=0;sub<tinfo->perm->N_samples_padded;sub++,p_in++)
          //  sum+=p_perm[sub]*(*p_in);

          __asm__ __volatile__(
            "movl %4,%%ecx                       \n\t" // counter
            "vxorpd %%ymm1,%%ymm1,%%ymm1         \n\t" // zero
            "movq %1, %%rdi                      \n\t" // sum (for p_out, eventually)
            "movq %2, %%rsi                      \n\t" // p_in
            "movq %3, %%r12                      \n\t" // p_perm_ (mul)

            "5:                                     "
            "vmovaps (%%r12),%%ymm3              \n\t" // mul
            "VFMADD231PS (%%rsi), %%ymm3, %%ymm1 \n\t" // rsi was ymm2

            "addq $0x20, %%r12                   \n\t" // address +32 bytes
            "addq $0x20, %%rsi                   \n\t"
            "dec %%ecx                           \n\t"
            "jnz 5b                              \n\t"

            "vmovaps %%ymm1, (%%rdi)             \n\t" // result
            : "=m" (sum_v)
            : "m" (sum_v), "m" (p_in_), "m" (p_perm_), "r" (ixi) // ix should be long, perhaps?
            : "ymm1", "ymm2", "ymm3", "rsi", "rdi","r12", "ecx","memory"
          );

          register float fsum=0.0;

          // sum_ contains ATATIME elements from the parallel additions; merge these to obtain final sum
          for (register int i=0;i<ATATIME;i++) {
            fsum+=sum_[i];
          }
          // per subject }}}

          (*(p_out++))=fsum;

        } // per permutation }}}

        if (tinfo->thread_buf_bytes<(p_out-tinfo->thread_buf_out)*sizeof(float)) {
          printf("mode %d thread %02d ix %8ld perm %6d p_out orig: %p p_out_cur: %p sz: %ld diff: %ld     (%d:%d:%d * %d)\n",tinfo->mode, tinfo->thread_num,ix,-1,(void *)(tinfo->thread_buf_out),(void *)p_out,tinfo->thread_buf_bytes,(p_out-tinfo->thread_buf_out)*sizeof(float),tinfo->thread_perm0,tinfo->thread_permstep,tinfo->thread_permstop,ss);
        }
      } // per voxel (ix)
      break; /* }}} */

    case perm_sub_inner_vblendvps: // equivalent operation to perm_sub_inner, using VBLENDVPS masking for selection {{{
      for (long ix=0;ix<ss;ix++,p_out++) {
        sum_v=sum_;
        ixi=tinfo->perm->N_samples_padded;
        p_in=tinfo->thread_buf_in+ix*tinfo->perm->N_samples_padded;

        ixi=ixi-(ixi%ATATIME); // FIXME: currently MUST be a multiple of ATATIME
        assert(ixi%ATATIME==0);
        ixi/=ATATIME;
        assert(ixi>0);

        for (int perm=tinfo->thread_perm0;perm<tinfo->thread_permstop;perm+=tinfo->thread_permstep) { // per permutation {{{
          p_perm_f=tinfo->perm->perms_f+perm*tinfo->perm->N_samples_padded;
          p_perm_=(void *)p_perm_f;
          p_in_=(void *)p_in;

          ixi=tinfo->perm->N_samples_padded/ATATIME;

          // per-subject {{{
          // VBLENDVPS uses bits 31, 63, ... etc for masking: the sign bit. So,
          // can use p_perm_f (+/-1.0) directly.
          // "Conditionally copy single-precision floating-point values from
          // ymm2 or ymm3/m256 to ymm1, based on mask bits in the specified
          // mask register, ymm4."
          // Initialize ymm2 to 0.

          __asm__ __volatile__(
            "movl %4,%%ecx                       \n\t" // counter
            "vxorpd %%ymm2,%%ymm2,%%ymm2         \n\t" // zero ymm2, blend alternative
            "vxorpd %%ymm1,%%ymm1,%%ymm1         \n\t" // zero ymm1, sum
            "movq %1, %%rdi                      \n\t" // sum (for p_out, eventually)
            "movq %2, %%rsi                      \n\t" // p_in
            "movq %3, %%r12                      \n\t" // p_perm_ (mul)

            "5:                                     "
            "vmovaps (%%r12),%%ymm4              \n\t" // mul
            "VBLENDVPS %%ymm4, (%%rsi), %%ymm2, %%ymm3 \n\t" // rsi was ymm2
            "vaddps %%ymm3,%%ymm1,%%ymm1         \n\t" // add the things...

            "addq $0x20, %%r12                   \n\t" // address +32 bytes
            "addq $0x20, %%rsi                   \n\t"
            "dec %%ecx                           \n\t"
            "jnz 5b                              \n\t"

            "vmovaps %%ymm1, (%%rdi)             \n\t" // result

            : "=m" (sum_v)
            : "m" (sum_v), "m" (p_in_), "m" (p_perm_), "r" (ixi)
            : "ymm1", "ymm2", "ymm3", "ymm4", "rsi", "rdi","r12", "ecx","memory"
            );

          register float fsum=0.0;

          for (register int i=0;i<ATATIME;i++) {
            fsum+=sum_[i];
          }
          // per subject }}}

          (*(p_out++))=fsum;

        } /* per permutation }}} */
      } // per voxel (ix)

      break; /* }}} */

    case perm_sub_inner_vandps: // equivalent operation to perm_sub_inner, using VANDPS (PSLLD?) masking {{{

      for (long ix=0;ix<ss;ix++) {
        sum_v=sum_;
        ixi=tinfo->perm->N_samples_padded;
        p_in=tinfo->thread_buf_in+ix*tinfo->perm->N_samples_padded;

        assert(ixi%ATATIME==0);
        assert(ixi>0);

        p_perm_m32=tinfo->perm->perms_m32+(tinfo->perm->N_samples_padded*tinfo->thread_perm0);

        assert(sizeof(p_perm_m32[0])==4);

        for (int perm=tinfo->thread_perm0;perm<tinfo->thread_permstop; perm+=tinfo->thread_permstep) { // per permutation {{{
          p_perm_m32=tinfo->perm->perms_m32+perm*tinfo->perm->N_samples_padded;

#ifdef SHOW_DEBUG_INFO
          if (ix<3 && perm<10) {
            printf("%05ld:%05d: ",ix,perm);
            for (int pi=0;pi<80;pi++) {
              if (pi==tinfo->perm->N_samples) printf("|s|");
              if (pi==tinfo->perm->N_samples_padded) printf("|S|");
              if (pi==tinfo->perm->N_perms) printf("|p|");
              if (pi==tinfo->perm->N_perms_padded) printf("|P");
              printf(p_perm_m32[pi]?"X":"-");
            }
          printf("\n");
          }
#endif

          // Per subject {{{
          // for (int sub=0;sub<tinfo->thread_subs;sub++,p_in++): sum+=mul*(*p_in);

          p_perm_=(void *)p_perm_m32;
          p_in_=(void *)p_in;
          ixi=tinfo->perm->N_samples_padded/ATATIME;

          __asm__ __volatile__(
            "movl %4,%%ecx                       \n\t" // counter
            "vxorpd %%ymm1,%%ymm1,%%ymm1         \n\t" // zero ymm1, sum
            "movq %1, %%rdi                      \n\t" // sum (for p_out, eventually)
            "movq %2, %%rsi                      \n\t" // p_in
            "movq %3, %%r12                      \n\t" // p_perm_ (mul)

            "5:                                     "
            "vmovaps (%%r12),%%ymm4              \n\t" // 32-bit masks from p_perm_
            "VANDPS (%%rsi),%%ymm4,%%ymm3        \n\t" // mask out unused elements of p_in_
            "VADDPS %%ymm3,%%ymm1,%%ymm1         \n\t" // add the things...

            "addq $0x20, %%r12                   \n\t" // address +32 bytes: p_perm_
            "addq $0x20, %%rsi                   \n\t" //                    p_in_
            "dec %%ecx                           \n\t"
            "jnz 5b                              \n\t"

            "vmovaps %%ymm1, (%%rdi)             \n\t" // result to sum_v (via rdi)

            : "=m" (sum_v)
            : "m" (sum_v), "m" (p_in_), "m" (p_perm_), "r" (ixi)
            : "ymm1", "ymm2", "ymm3", "rsi", "rdi","r12", "ecx","memory"
          );

          register float fsum=0.0;

          for (register int i=0;i<ATATIME;i++) {
            /*
            if (!isfinite(sum_[i])) {
              printf("%d:%d :( ",ix,i);
            }*/
            fsum+=sum_[i];
          }

          /* }}} */

          (*(p_out++))=fsum;
        } // }}}

        if (tinfo->thread_buf_bytes<(p_out-tinfo->thread_buf_out)*sizeof(float)) {
          printf("mode %d thread %02d ix %8ld perm %6d p_out orig: %p p_out_cur: %p sz: %ld diff: %ld     (%d:%d:%d * %d)\n",tinfo->mode, tinfo->thread_num,ix,-1,(void *)(tinfo->thread_buf_out),(void *)p_out,tinfo->thread_buf_bytes,(p_out-tinfo->thread_buf_out)*sizeof(float),tinfo->thread_perm0,tinfo->thread_permstep,tinfo->thread_permstop,ss);
        }

      } // per voxel (ix)
      break; // }}}

    default:
      /* shouldn't get here. */
      assert(false);
  }

  gettimeofday(&tv_b,NULL);

  // gather stats...

  assert(tinfo->thread_permstep==1);
  assert(tinfo->thread_perm0==0);

  p_out=tinfo->thread_buf_out;

  //struct stat_result * sr=(struct stat_result *)malloc(ss*sizeof(struct stat_result));
  if ((tinfo->sr0!=NULL)&&(tinfo->sr1!=NULL)) {
    assert(tinfo->perm->N_conditions==2);
    for (long ix=0;ix<ss;ix++) {
      //printf("%d/%d :",ix,ss); fflush(stdout);
      //printf("N_perms %d padded %d\n", tinfo->perm->N_perms, tinfo->perm->N_perms_padded);

      // at least in modes 0,7: inner index is perm; there are N_perms_padded of them per voxel. so, stepsize 1, step thread_buf_out by N_perms_padded for each ix.

      bool dbg=(ix>ss/8)&&(ix<(ss/8)+10); // show debug info for a subset of items
      perm_stats_worker(tinfo->sr0+ix, p_out[0], tinfo->perm->N_perms, p_out+tinfo->perm->N_conditions, tinfo->thread_num, tinfo->thread_count, 1,dbg); // use unpadded perms for the stats
      perm_stats_worker(tinfo->sr1+ix, p_out[1], tinfo->perm->N_perms, p_out+tinfo->perm->N_conditions, tinfo->thread_num, tinfo->thread_count, 1,dbg);
      p_out+=tinfo->perm->N_perms_padded;
    }
  }
  else {
    printf("Nowhere to put the stats.\n");
  }

  gettimeofday(&tv_c,NULL);
  sprintf(buf,"%2.2fs permute, %2.2fs stats\n", time_diff_secs(&tv_a,&tv_b), time_diff_secs(&tv_b,&tv_c));

//#ifdef SHOW_DEBUG_INFO
  printf(buf);
//#endif

  return strdup(buf);
} // }}} perm_processing_thread


void run_perm_threads(int num_threads, long slice_size, long total_size, struct perm_info * pi, enum perm_modes mode, float * vol_data, struct stat_result ** stat_result_ptrs ) { // {{{
  // Slice up the vol_data, and process in up to num_threads threads

  struct perm_thread_info *tinfo;
  struct timeval tv_in, tv_out;

  int            s;
  pthread_attr_t attr;
  void          *res;

  long           mem_pages = sysconf(_SC_PHYS_PAGES);
  long           mem_page_size = sysconf(_SC_PAGE_SIZE);
  long           mem_size=mem_pages * mem_page_size;
  static long    mem_max=0;


  float         *buf;
  float         *buf_perm;
  long           required_slices;
  long           perm_buf_size;

  long           bs;
  long           sub_buf_size;

  // Somewhat arbitrarily, set a ceiling of 60% physical memory for the two most memory-demanding buffers (sub_buf and perm_buf)
  if (mem_max==0) {
    mem_max=(3*mem_size)/5;
  }

  do { // tune memory utilisation; allocate major working buffers {{{

    // adjust slice_size and num_threads to ensure we can fit everything in memory... {{{
    long slice_size_max_adjusted=0;
    long slice_size_adjusted;
    int num_threads_adjusted;

    slice_size_adjusted=slice_size;
    num_threads_adjusted=num_threads;

    while ((slice_size_adjusted>2048)||(num_threads_adjusted>1)) {
      slice_size_max_adjusted=(mem_max/sizeof(float))/(pi->N_perms_padded*num_threads_adjusted+pi->N_samples_padded*num_threads_adjusted);
      printf("ssa %ld nta %d ssma %ld\n", slice_size_adjusted, num_threads_adjusted, slice_size_max_adjusted);
      if (slice_size_adjusted<slice_size_max_adjusted) {
        break;
      }
      if ((slice_size_adjusted>8192)||(num_threads_adjusted<6)) {
        slice_size_adjusted=PADDED_SIZE(slice_size_adjusted/2);
      }
      else if (num_threads_adjusted>1) {
        num_threads_adjusted--;
      }
      // but try to avoid the unfortunate situation of having one short, lonely
      // thread to pick up the tail:
      if (((total_size%slice_size_adjusted)<(slice_size_adjusted/2))&& ((total_size/slice_size_adjusted)%num_threads<2) && (slice_size_adjusted>4096)) {
        printf("Attempting to correct poor choice of slice size.\n");
        slice_size_adjusted=PADDED_SIZE(slice_size_adjusted+slice_size_adjusted/4);
      }
    }

    if ((num_threads_adjusted<num_threads)||(slice_size_adjusted<slice_size)) {

      printf("Due to memory constraints, adjusted slice size %ld => %ld, threads %d => %d\n", slice_size, slice_size_adjusted, num_threads, num_threads_adjusted);

      slice_size=slice_size_adjusted;
      num_threads=num_threads_adjusted;
    }

    // }}}

    // required_slices : also ensure we get good CPU utilization {{{

    required_slices=total_size/slice_size;

    if (required_slices<num_threads) {
      printf("Requested slice size would only utilize %ld threads; adjusting slice size downwards.\n",required_slices);
      slice_size=PADDED_SIZE(total_size/(num_threads-1));
      required_slices=total_size/slice_size;
    }
    // adjust for rounding errors in slice count calculation (possible incomplete final slice)...
    if ((required_slices*slice_size)<total_size) {
      required_slices++;
    }

    // }}}

    assert(slice_size<slice_size_max_adjusted);

    perm_buf_size=pi->N_perms_padded*slice_size*sizeof(float)*num_threads;

    bs=pi->N_samples_padded*slice_size*sizeof(float);
    sub_buf_size=mem_max-perm_buf_size;

    long sub_buf_required=num_threads*bs;

    printf("perm_buf_size %ld, sub_buf_required %ld, mem_max %ld, sub_buf_size %ld (req slice size %ld, slice_size_max_adjusted %ld)\n", perm_buf_size, sub_buf_required, mem_max, sub_buf_size, slice_size, slice_size_max_adjusted);

    assert(sub_buf_size>=sub_buf_required);

    sub_buf_size=sub_buf_required;

    printf("Allocating %ld (sub) + %ld (perm) bytes\n", sub_buf_size, perm_buf_size);

    buf=aligned_alloc(ALIGN_AS, sub_buf_size);

    if (buf!=NULL) {
      // Subject buffer acquired successfully; now try perm_buffer
      buf_perm=aligned_alloc(ALIGN_AS, perm_buf_size);
      if (buf_perm!=NULL) {
        // also ok; good to proceed
        break;
      }
      else {
        // couldn't get perm buf, so get rid of the sub buf and loop again with smaller mem_max
        free(buf);
        buf=NULL;
      }
    }

    mem_max/=2;
    printf("Buffer allocation failed; trying again with new max_mem: %ld\n", mem_max);

  } while (mem_max>1e9L);
  // }}}

  assert(buf!=NULL);
  assert(buf_perm!=NULL);

  printf("Initializing memory...\n");

  if (vol_data==NULL) {
    printf("Writing all 0s to subject input buffer... specify vol_data argument to use real data!\n");
  }

  memset(buf,0,sub_buf_size);
  memset(buf_perm,0,perm_buf_size);

  printf("Total size: %ld, slice size %ld, required slices %ld\n", total_size,slice_size,required_slices);

  // Initialize thread creation attributes

  s = pthread_attr_init(&attr);

  if (s != 0)
    handle_error_en(s, "pthread_attr_init");

  // Allocate memory for thread info structures, one per thread

  tinfo = calloc(MAX_THREADS, sizeof(struct perm_thread_info));

  if (tinfo == NULL)
    handle_error("calloc");

#ifdef FAKE_THREADS
  printf("Faking threads.\n");
#endif

#ifdef CHECK_PERM_CONSISTENCY
  float * perm_std=NULL;
  float * perm_ref=NULL;
#endif

  gettimeofday(&tv_in,NULL);

  for (int cur_slice=0;cur_slice<required_slices; ) { // {{{
    int cur_thread;
    int num_threads_this_iteration=0;
    long ps=slice_size*pi->N_perms_padded*sizeof(float);

    // create up to num_threads threads
    for (cur_thread=0; cur_thread<num_threads && cur_slice<required_slices; cur_slice++, cur_thread++, num_threads_this_iteration++ ) {
      // start individual threads, one per slice
      tinfo[cur_thread].thread_num = cur_thread;
      tinfo[cur_thread].thread_count=num_threads;

      if (stat_result_ptrs!=NULL) {
        tinfo[cur_thread].sr0=stat_result_ptrs[0]+cur_slice*slice_size;
        tinfo[cur_thread].sr1=stat_result_ptrs[1]+cur_slice*slice_size;
      }

      tinfo[cur_thread].thread_buf_out=buf_perm+(cur_thread*(ps/sizeof(float)));
      tinfo[cur_thread].thread_buf_bytes=ps;

      assert(slice_size<INT_MAX); // else we will have trouble.
      tinfo[cur_thread].thread_slice_size=slice_size;

      tinfo[cur_thread].perm=pi;

      tinfo[cur_thread].thread_buf_in=buf+(cur_thread*bs/sizeof(float)); // bs already has sizeof(float) factored in

      if (vol_data!=NULL) {
        // Transfer slices of (unaligned) source data into the aligned input
        // buffer for processing
        printf("Preparing slice %d: ", cur_slice);
        fflush(stdout);
        long pts_real=0;
        long pts_pad=0;
        float *cp=tinfo[cur_thread].thread_buf_in;
        int in_ofs=cur_slice*slice_size;
        int this_slice_size=slice_size;
        if (total_size<in_ofs+slice_size) {
          this_slice_size=total_size-in_ofs;
          tinfo[cur_thread].thread_slice_size=this_slice_size;
          printf("Slice %d size too great; adjusting from %ld down to %d\n", cur_slice,slice_size,this_slice_size);
        }
        for (int sx=0;sx<this_slice_size;sx++) {
          for (int sub=0;sub<pi->N_samples;sub++) {
            (*(cp++))=vol_data[in_ofs+sx+total_size*sub];
            pts_real++;
          }
          // zero padding (for aligned samples)
          for (int sub=pi->N_samples;sub<pi->N_samples_padded;sub++) {
            (*(cp++))=0;
            pts_pad++;
          }
        }
        // padding (for the final, possibly incomplete slice)
        for (int sx=this_slice_size;sx<slice_size;sx++) {
          for (int sub=0;sub<pi->N_samples_padded;sub++) {
            pts_pad++;
            (*(cp++))=0;
          }
        }
        printf("Copied %ld points; padded %ld points (N_samples %d padded to %d)\n", pts_real, pts_pad, pi->N_samples,pi->N_samples_padded);
      }

      tinfo[cur_thread].thread_perm0=0;
      tinfo[cur_thread].thread_permstop=0;
      tinfo[cur_thread].thread_permstep=1;


      tinfo[cur_thread].mode=mode;

#ifdef FAKE_THREADS
      // Run thread procedure here, in the current thread

      printf("Thread %02d, %p (buf=%p)\n", cur_thread, tinfo[cur_thread].thread_buf_in, buf);
      res=perm_processing_thread(&(tinfo[cur_thread]));
      printf("Thread %02d info: %p\n", cur_thread, res);
      free(res);

#ifdef CHECK_PERM_CONSISTENCY // debugging mode, re-run with perm_sub_inner mode to validate output {{{

      if (mode!=perm_sub_inner) {
        printf("Consistency check: repeating with standard (mode=perm_sub_inner) permutation loop:\n");
        perm_std=tinfo[cur_thread].thread_buf_out;
        perm_ref=aligned_alloc(ALIGN_AS,ps);
        assert(perm_ref!=NULL);

        tinfo[cur_thread].thread_buf_out=perm_ref;
        tinfo[cur_thread].mode=perm_sub_inner;

        res=perm_processing_thread(&(tinfo[cur_thread]));

        printf("Thread %02d info: %p\n", cur_thread, res);
        free(res);

        long     n_veryclose=0;
        long     n_similar=0;
        long     n_different=0;
        double   cumulative_abs_std=0;
        double   cumulative_abs_ref=0;
        double   cumulative_err=0;
        double   this_err=0;
        double   worst=0;
        long     notfinite=0;

        for (long _ix=0;_ix<(ps/sizeof(float));_ix++) {
          cumulative_abs_std+=fabs(perm_std[_ix]);
          cumulative_abs_ref+=fabs(perm_ref[_ix]);

          this_err=fabs(perm_std[_ix]-perm_ref[_ix]);

          if (!isfinite(perm_std[_ix])) {
            notfinite++;
          }
          else {
            if (this_err>worst) {
              worst=this_err;
            }
            if (this_err<1e-5) {
              n_veryclose++;
            }
            else if (this_err<1e-3) {
              n_similar++;
            }
            else {
              n_different++;
            }
          }
          cumulative_err+=this_err;
        }

        free(perm_ref);
        printf("Almost equal: %ld, similar %ld, not similar %ld err %.2f%% typical %e / %e (%e) worst %e invalid %ld\n", n_veryclose,n_similar, n_different,(100.0*n_different)/((float)(ps/sizeof(float))),cumulative_err/(ps/sizeof(float)),cumulative_abs_ref/((ps/sizeof(float))),cumulative_abs_std/((ps/sizeof(float))), worst, notfinite);
      }

#endif // CHECK_PERM_CONSISTENCY }}}

      res=NULL;

#else // not FAKE_THREADS; actually create separate threads {{{

      if (num_threads>1) {
        s = pthread_create(&tinfo[cur_thread].thread_id, &attr, &perm_processing_thread, &tinfo[cur_thread]);
      }
      else {
        res=perm_processing_thread(&(tinfo[cur_thread])); /* it is freed, ~29 lines down... */
      }

      if (s != 0) {
        handle_error_en(s, "pthread_create");
      }

#endif // !FAKE_THREADS }}}

    } /* cur_thread... */

#ifndef FAKE_THREADS // attach all the separate threads {{{
    if (num_threads>1) {
      /* attach the threads... */
      for ( cur_thread=0; cur_thread<num_threads_this_iteration; cur_thread++) {
        //printf("-%02d",cur_thread); fflush(stdout);
        s = pthread_join(tinfo[cur_thread].thread_id, &res);
        if (s != 0)
          handle_error_en(s, "pthread_join");

        if (res!=NULL) {
          printf("Thread returned: %s\n", (char *)res);
          free(res);
        }
      }

    }
    else if (res!=NULL) {
      printf("Thread returned: %s\n", (char *)res);
      free(res);
    }
#endif // !FAKE_THREADS }}}

    // periodically report on progress
    if (required_slices<=2048||!(cur_slice%(10*num_threads))) {
      gettimeofday(&tv_out,NULL);
      long eta=cur_slice>0?((required_slices-cur_slice)*(long)(tv_out.tv_sec-tv_in.tv_sec))/cur_slice:999;
      printf("%5d/%5ld, eta %4lds, est total %4lds\r",cur_slice,required_slices,eta,eta+(long)(tv_out.tv_sec-tv_in.tv_sec));
      fflush(stdout);
    }
  } // cur_slice }}}

  gettimeofday(&tv_out,NULL);
  printf("\nthreads %d slice_size %ld req_slices %ld buf_size %ld subs %d (pad to %d) perms %d (pad to %d) ", num_threads,slice_size,required_slices, bs, pi->N_samples, pi->N_samples_padded, pi->N_perms, pi->N_perms_padded);
  printf("mode %d time: %5.2fs\n", mode, 1e-6*((float)(tv_out.tv_usec-tv_in.tv_usec))+(tv_out.tv_sec-tv_in.tv_sec));

  free(buf);
  free(buf_perm);

} // run_perm_threads }}}

// threaded permutation processing }}}



int main(int argc, char * argv[]) { // {{{
  FSLIO         *fslio;
  FSLIO         *fslo;
  char          *f1name;
  char          *output_prefix="test";

  if (argc > 2) {
    output_prefix=argv[2];
  }

  printf("Output prefix: %s\n", output_prefix);
  printf("available handles: %d\n", n_handles_available());

#ifdef CACHE_HANDLES
  struct rlimit flim;

#define DESIRED_RLIM (2*CACHE_HANDLES)
  getrlimit(RLIMIT_NOFILE, &flim);
  printf("Initial limit on files: %ld/%ld\n",flim.rlim_cur,flim.rlim_max);

  if (flim.rlim_cur<DESIRED_RLIM) {
    if (flim.rlim_max>=DESIRED_RLIM) {
      flim.rlim_cur=DESIRED_RLIM;
      if (0==setrlimit(RLIMIT_NOFILE, &flim)) {
        printf("Adjusted limit on files: %ld/%ld\n",flim.rlim_cur,flim.rlim_max);
      }
      else {
        printf("Adjustment failed.\n");
      }
    }
    else {
      printf("\nSystem rlimit settings prevent holding all source image files open; this will\nhurt performance.\n\n");
      printf("You may be able to verify this with \"ulimit -H -n\", and may be able to change it\n");
      printf("in one of these files (look for the NOFILE parameter).\n\n");
      printf("/etc/security/limits.conf /etc/systemd/system.conf /etc/systemd/user.conf\n\n");
      printf("For /etc/security/limits.conf, try adding a line like:\n  your_username hard nofile 15000\n...then log in again for the change to take effect.\n");
    }
  }
#endif // CACHE_HANDLES

  long           slice_size=1<<15; // 16?
  struct perm_info * pi=NULL;

// Disable GSL's conservative, failsafe default error handling which aborts
// execution at the first sign of anomolies. We use a subset of GSL functions
// which return meaningful error codes, and try to handle these sensibly.

  gsl_set_error_handler_off();

  // Process commandline parameters:

  if (argc < 3) {
          pusage(argv[0]);
          exit(1);
  }

  // mask filename
  f1name = argv[1];

  // first and (optional) second namelist
  char         **nl1=NULL;
  char         **nl2=NULL;

  if (argc>3) {
    nl1=read_namelist(argv[3]);
  }

  if (argc>4) {
    nl2=read_namelist(argv[4]);
  }

  int            num_subjects=0;
  char         **np=nl1;

  for (np=nl1;*np!=NULL;np++)
    num_subjects++;

  for (np=nl2;*np!=NULL;np++)
    num_subjects++;


  printf("Total subjects: %d\n",num_subjects);

  // read initial mask

  fslio = FslReadHeader(f1name);

  long           vol_pts=FslGetVolSize(fslio);

  float         *mask=read_volume_masked(f1name,NULL,NULL,0,0,-1);

  long           vol_pts_inmask=0;

  float         *fp=mask;

  for (int i=0;i<vol_pts;i++) {
    if (*(fp++)) vol_pts_inmask++;
  }

  printf("mask: %ld/%ld, %.1f%%\n", vol_pts_inmask, vol_pts, vol_pts_inmask*100.0/vol_pts);

  float *        all_subject_data=(float *)malloc(2*num_subjects*vol_pts_inmask*sizeof(float)); // no need for aligned alloc here, this will be re-ordered later. 2* to support background loading

  printf("Template from vol %s\n", *nl1);
  FSLIO * sample_functional=FslReadHeader(*nl1); // header from first volume in first list is considered as representative of all...

  int total_volumes=sample_functional->niftiptr->nt;


  struct stat_result * stats_this_volume=(struct stat_result *)malloc(vol_pts_inmask*sizeof(struct stat_result)*2); // first half for sublist1, second for sublist2 (inverse case)
  struct stat_result * stats_ptrs[2];
  stats_ptrs[0]=stats_this_volume;
  stats_ptrs[1]=stats_this_volume+vol_pts_inmask;

  // allocate memory for full set of 12 output images
  int n_outputs=12;
  float ** all_4d_outputs=malloc(n_outputs*sizeof(float *));

  for (int i=0;i<n_outputs;i++) {
    all_4d_outputs[i]=(float *)malloc(total_volumes*vol_pts*sizeof(float));
    memset(all_4d_outputs[i],0,total_volumes*vol_pts*sizeof(float));
  }

  // setup info for loader. 
  // ltix saves a copy of details from the previous load (for potential validation)
  struct loader_thread_info lti, ltix;

  lti.num_subjects=num_subjects;
  lti.nl1=nl1;
  lti.nl2=nl2;
  lti.total_volumes=total_volumes;
  lti.all_subject_data=all_subject_data;
  lti.vol_pts_inmask=vol_pts_inmask;
  lti.mask=mask;

#ifdef BACKGROUND_LOAD
  pthread_attr_t attr;

  int s = pthread_attr_init(&attr);

  if (s != 0) {
    handle_error_en(s, "pthread_attr_init");
  }
#endif // BACKGROUND_LOAD

  for (int cur_vol=-1; cur_vol<total_volumes; cur_vol++) { // iterate over volumes {{{

    if (cur_vol<0) {
      // for cur_vol<0 files are initially loaded, and permutation structures allocated
      printf("Validating input...");
    }
    else {
      printf("Loading volume %d of %d", cur_vol, total_volumes);
    }

    int sub=0;

    if (cur_vol<0) { // before doing any actual work: check that the source data can be loaded, build the permutation structure and set initial grouping {{{

      lti.cur_vol=cur_vol;
      lti.validate=false;
      loader_batch_run(&lti);

#ifdef AC_QUICK
      pi=perm_info_build(1000,num_subjects,2); // perms, samples, conditions
#else
      pi=perm_info_build(10000,num_subjects,2);
#endif
      sub=0;

      for (np=nl1;*np!=NULL;np++) {
        assert(sub<num_subjects);
        perm_info_set(pi,0,sub,1);
        perm_info_set(pi,1,sub,0);
        sub++;
      }
      for (np=nl2;*np!=NULL;np++) {
        assert(sub<num_subjects);
        perm_info_set(pi,0,sub,0);
        perm_info_set(pi,1,sub,1);
        sub++;
      }
      long n_00=0;
      long n_01=0;
      long n_10=0;
      long n_11=0;
      for (sub=0;sub<num_subjects;sub++) {
        if (pi->perms_m32[perm_info_ix(pi,0,sub,false)]) {
          n_01++;
        }
        else {
          n_00++;
        }
        if (pi->perms_m32[perm_info_ix(pi,1,sub,false)]) {
          n_11++;
        }
        else {
          n_10++;
        }
      }
      printf("Grouping: %ld %ld %ld %ld\n", n_00,n_01,n_10,n_11);
    } // preparation }}}
    
    else { // for all the regular volumes (after initialization) {{{

      // Load some data {{{

      int bg_loader=false;

#ifdef BACKGROUND_LOAD
      // For the first batch of real data: load now, in foreground
      if (cur_vol==0) {
        lti.cur_vol=cur_vol;
        loader_batch_run(&lti);
      }

      memcpy(&ltix,&lti,sizeof(struct loader_thread_info));

      // For all but the last batch, background load the next dataset:
      if (cur_vol+1<total_volumes) {
        lti.cur_vol=cur_vol+1;
        s = pthread_create(&(lti.thread_id), &attr, &loader_batch_run, &lti);
        bg_loader=true;
        if (s != 0) {
          handle_error_en(s, "pthread_create");
        }
      }
#else // BACKGROUND_LOAD
      // No background loading? Load right now.
      lti.cur_vol=cur_vol;
      loader_batch_run(&lti);
#endif // !BACKGROUND_LOAD

      // }}}

      // the current volumes are loaded; now, start the actual slicing and processing:
      run_perm_threads((6*MAX_THREADS)/7, slice_size, vol_pts_inmask, pi, PERM_FUNCTION, all_subject_data+((cur_vol%2)*num_subjects*vol_pts_inmask), stats_ptrs);

      // cleanup perm_stats internal buffers and show diagnostic info
      perm_stats_cleanup();

#ifdef CHECK_LOAD_CONSISTENCY
      printf("Checking input buffer for consistency, after processing:\n");
      ltix.validate=true;
      loader_batch_run(&ltix);
      ltix.validate=false;
#endif

      // write intermediate output (after each completed volume) {{{

      // the first image from the first list will be used as a header template
      FSLIO * template_image=FslOpen(*nl1,"r"); 

      int nfailed=0;

      for (int n_output=0;n_output<n_outputs;n_output++) {

        // Output files:
        //
        // n_output=
        //
        //  0, 1  Test statistic (group mean) for group 0, group 1
        //
        //  2     Distribution mean
        //     3  Distribution SD
        //
        //  4, 5  Difference (statistic-dist mean) for group 0, group 1
        //
        //  6, 7  P-value from random permutation testing, one-sided
        //
        //  8, 9  P-value from gamma approximation of the distribution
        //
        // 10,11  Q (1-P) value equivalents for 8,9. These are for convenience
        //        of visualisation; due to the nature of the floating-point
        //        representation, numerical precision may be lower than for the
        //        P-value outputs.

        printf("Generating intermediate output %d (volume %d of %d):  ", n_output, cur_vol, total_volumes);
        fflush(stdout);

        char f2n[256];
        char out_base[180];
        snprintf(out_base,180,"%s",output_prefix);
        out_base[179]='\0';

        snprintf(f2n,255,"%sim%03d.nii",out_base,n_output);
        f2n[255]='\0';

        struct stat_result * sp;
        float * op=all_4d_outputs[n_output]+vol_pts*cur_vol;

        int what=n_output/2;
        int group=n_output%2;
        sp=stats_ptrs[group];

        long wrote_nonfinite=0;
        long wrote_finite=0;
        long wrote_not_zero=0;
        long wrote_abnormal=0;
        double got_max=-1;

        int first=1;

        for (int mx=0;mx<vol_pts;mx++,op++) { /* extract relative details, expanding from mask to full vol in the process */
          if (mask[mx]) { // unmasking happens here
            if (what==0) { /* group means */
              *op=(sp++)->sample; // 0,1
              if (first) sprintf(f2n,"%sim%03d_mean%01d.nii",out_base,n_output,group);
            }
            else if (what==1) {
              if (group==0) { /* group is irrelevant for mean/sd, so use this factor to split the two instead */
                if (first) sprintf(f2n,"%sim%03d_mean.nii",out_base,n_output);
                *op=(sp++)->mean; // 2
              }
              else {
                if (first) sprintf(f2n,"%sim%03d_sd.nii",out_base,n_output);
                *op=(sp++)->sd; // 3
              }
            }
            else if (what==2) {
              if (first) sprintf(f2n,"%sim%03d_diff%01d.nii",out_base,n_output,group);
              *op=sp->sample-sp->mean; // 4,5
              sp++;
            }
            else if (what==3) {  // 6,7
              if (first) sprintf(f2n,"%sim%03d_p%01d.nii",out_base,n_output,group);
              *op=(sp++)->p;
            }
            else if (what==4) { // 8,9
              if (first) sprintf(f2n,"%sim%03d_p_ext%01d.nii",out_base,n_output,group);
              *op=(sp++)->p_tail;
            }
            else if (what==5) { // 10,11
              if (first) sprintf(f2n,"%sim%03d_Qsigned%01d.nii",out_base,n_output,group);
              if (sp->sample<sp->mean)
                *op=-(1.0-(sp++)->p_tail);
              else
                *op=(1.0-(sp++)->p_tail);
            }
            first=0;
          }
          else {
            *op=0;
          }
          if (!isfinite(*op)) {
            wrote_nonfinite++;
          }
          else {
            wrote_finite++;
            if (!isnormal(*op)) {
              wrote_abnormal++;
            }
            if (fabs(*op)>1e-12) {
              wrote_not_zero++;
            }
            else {
              if (fabs(*op)>got_max) {
                got_max=fabs(*op);
              }
            }
          }
        }

        fslo = FslOpen(f2n,"wb");
        FslCloneHeader(fslo,template_image);
        FslWriteHeader(fslo);
        FslWriteVolumes(fslo,all_4d_outputs[n_output],total_volumes);
        FslClose(fslo);

        printf("Wrote to %s (%ld nonzero, %ld non-NaN, %ld NaN %ld not normal)\n",f2n, wrote_not_zero,wrote_finite,wrote_nonfinite,wrote_abnormal);
        if (wrote_not_zero==0) {
          printf("Abs max value: %e\n",got_max);
        }

        // make sure we're not writing complete nonsense...
        if ((wrote_not_zero==0)||(wrote_finite==0)) {
          nfailed++;
        }
        //assert(wrote_not_zero>0);
        //assert(wrote_finite>0);
      }

      FslClose(template_image);

      assert(nfailed==0);

      // }}}

#ifdef BACKGROUND_LOAD
      // finally, check on the background loader thread; must be finished before proceeding to the next iteration {{{
      if (bg_loader) {
        void * res;
        s = pthread_join(lti.thread_id, &res);
        if (s != 0)
          handle_error_en(s, "pthread_join");
        if (res!=NULL) {
          printf("Loader returned: %s\n", (char *)res);
          free(res);
        }
      }
      // }}}
#endif // BACKGROUND_LOAD

    } // regular volumes }}}

  } // cur_vol }}}

#ifdef BACKGROUND_LOAD
  s = pthread_attr_destroy(&attr);
  if (s != 0)
    handle_error_en(s, "pthread_attr_destroy");
#endif // BACKGROUND_LOAD

  exit(0);
} // }}}


void pusage(char *cmd) { // {{{
  // display usage info
  fprintf(stderr, "\n%s performs random permutation analysis between two sets of 4D imaging data.\n\n",cmd);
  fprintf(stderr, "Usage: %s <mask> <prefix> <list1> <list2>\n\n",cmd);
  fprintf(stderr, "\tmask\t: Filename for input mask image (eg: mask.nii.gz)\n");
  fprintf(stderr, "\tprefix\t: Filename prefix for output imagery (eg: r4d_output)\n");
  fprintf(stderr, "\tlist1\t: Text file containing list of source images, one per line,\n");
  fprintf(stderr, "\t     \t  for the first group (eg: list1.txt)\n");
  fprintf(stderr, "\tlist2\t: Text file containing list of source images for the second\n\t\t  group, one per line (eg: list2.txt)\n\n");
  fprintf(stderr, "Source images must be in NIFTI format (optionally gzipped), having identical dimensions with 32-bit float (4-byte) samples.\n");
  fprintf(stderr, "\n");
  return;
} // }}}
